<?php
/* @var $this LifeeventController */
/* @var $Details LifeEvent */
/* @var $relatedData array */
?>
<div class="panel panel-group" id="accordion2">
    <h1> 
        <?php echo $Details->Name; 
              /* if (Yii::app()->getModule('user')->isAdmin()) {
                    echo '<button id="dP0" class="btn btn-danger pull-right" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-trash"></i><span>  </span>Delete</button>';
              } */
          ?>
    </h1>
</div>

<div class="modal fade" id="myModal">
  <div class="modal-dialog alert-danger">
    <div class="modal-content alert-danger">
      <div class="modal-header alert-danger">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h1><span class="glyphicon glyphicon-exclamation-sign"></span> Are you sure?</h1>
      </div>
      <div class="modal-body">
          <h4>Deleting this life event record is irreversibly permanent</h4>
          <h6>Only continue if you are sure you wish to delete this record</h6>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="dP" class="btn btn-danger pull-right" data-dismiss="modal"><i class="glyphicon glyphicon-trash"></i><span>  </span>Delete this life event</button>
      </div>
    </div>
  </div>
</div>

<div class="panel panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          Edit Details
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel panel-collapse collapse">
      <div class="panel panel-body">
        <?php
            $this->widget('bootstrap.widgets.TbEditableDetailView', array(
                'id' => 'editable',
                'data' => $Details,
                'url' => 'lifeevent/update',
                'attributes' => array(
                    'Name',
                    'Notes'
                ),
            ));
        ?>
      </div>
    </div>
  </div>
<<<<<<< HEAD
    <div class="panel panel-default">
    <div class="panel panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
          People
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel panel-collapse collapse">
      <div class="panel panel-body">
        <?php
            if ($relatedData["people"]->getItemCount() < 1) {
                echo '<div class="alert alert-info" role="alert"><strong>Information: </strong> No one has had this life event</div>';
            }
            else {
                $this->widget('bootstrap.widgets.TbGridView', array(
                    'id' => "pplGrid",
                    'type' => 'striped bordered',
                    'dataProvider' => $relatedData['people'],
                    'selectableRows' => true,
                    'htmlOptions' => array(
                        'style' => 'padding-bottom: 10px'
                    ),
                    'columns' => array(
                        array(
                            'name' => 'Name',
                            'value' => '$data->fullName'
                        ),
                        array(
                            'name' => 'With',
                            'value' => function ($data) use ($Details) {
                                                if ($Details->Name === "Marriage") {
                                                    $criteria = new CDbCriteria();
                                                    $criteria->addCondition('Person = ' . $data->ID . " AND Position = 'Parent'");
                                                    $fm = FamilyMembership::model()->find($criteria);
                                                    $criteria = new CDbCriteria();
                                                    $criteria->addCondition('Family = ' . $fm->Family . ' AND Position = "Parent" AND Person != ' . $data->ID);
                                                    $fm = FamilyMembership::model()->find($criteria);
                                                    $p = Person::model()->findByPk($fm->Person);
                                                    return $p->fullName;
                                                }
                                                else {
                                                    return 'N/A';
                                                }}
                        ),
                        array(
                            'name' => 'Address',
                            'value' => '$data->FullAddress'
                        ),
                        array(
                            'name' => 'City',
                            'value' => '$data->City'
                        ),
                        array(
                            'name' => 'State',
                            'value' => '$data->State'
                        ),
                        array(
                            'name' => 'ZIP',
                            'value' => '$data->ZIP'
                        ),
                        array(
                            'htmlOptions' => array('nowrap'=>'nowrap'),
                            'class'=>'bootstrap.widgets.TbButtonColumn',
                            'buttons' => array(
                                'view' => array(
                                    'visible' => function($row,$data){false;},
                                ),
                                'update' => array(
                                    'visible' => function($data,$row){false;},
                                ),
                                'delete' => array(
                                    'url' => function($data) use ($Details){
                                                return Yii::app()->createUrl('/lifeevent/deleteLinkRecord',
                                                array('thisID'=>$Details->ID, 'linkData'=>$data->ID, 'linkName'=>'Person', 'Entity'=> 'LifeEvent')
                                                );
                                            },
                                    'visible' => function($data,$row){return Yii::app()->getModule('user')->isAdmin();},
                                )
                            )
                    )
                    )
                ));
            }
            
            if ($relatedData['availablePeople'] !== NULL) {
                $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
                'id' => 'newLEO-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                    'validateOnType' => true,
                ),
                'htmlOptions' => array('class' => 'well'),
                    )
                );
                echo '<div class="form-group">';
                echo '<h4>Add a new family member</h4>';
                $list = CHtml::listData($relatedData['availablePeople'], 'ID', 'fullName');
                echo CHtml::dropDownList('personDrop', '', $list, array('prompt' => 'Select a Person', 'class' => 'input-large form-control'));
                echo "<input class='form-control datepicker' placeholder='Date of Occurrance' data-provide='datepicker' id='occurranceDate'>";
                echo '<div class="form-actions" style="padding-top: 10px">';
                echo '<button class="btn btn-primary" id="newLEO" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i><span>  </span>Save</button>';
                echo '<button class="btn btn-default" type="reset"><i class="glyphicon glyphicon-repeat"></i><span>  </span>Reset</button>';
                echo '</div>';
                echo '</div>';
                $this->endWidget();
            }
            else {
                echo '<div class="alert alert-info" role="alert"><strong>Information: </strong> All available people are in this family</div>';
            }
        ?>
      </div>
    </div>
  </div>
=======
    
>>>>>>> origin/cm_unstable
</div>


<script>
    $(function(){
        $('#dP').click(function(){
                $('#myModal').on('hidden.bs.modal', function (e) {
                       var pK = <?php echo $Details->ID; ?>;
                       var jqxhr = $.ajax( {
                        type: 'POST',
                        url: 'lifeevent/delete',
                        data: { pk: pK },
                        })
                    .done(function(data) {
                        $('#updateData').html(data);
                        $('#accordionDel').collapse();
                        $.notify("Successfully Deleted", "success");
                        $.fn.yiiGridView.update('mainLEGrid');
                    })
                    .fail(function() {
                       console.log("Deleting the life event occurrance failed. Check model, controller and database");
                    })
                })
        });
        $('body').on('click', '#newLEO', function(e){
            e.preventDefault();
            var personElement = document.getElementById("personDrop");
            var Select = personElement.options[personElement.selectedIndex].value;
            var selPos = document.getElementById("occurranceDate").value;
            var jqxhr = $.ajax({
                type: 'POST',
                url: 'lifeevent/newLEO',
                data: {Person: Select,
                       LifeEvent: <?php echo $Details->ID; ?>,
                       Date: selPos},
                cache: false
            })
            .done(function() {
                $.notify("Added a new life event occurrance", "success");
            })
            .fail(function() {
                console.log("Unable to add a new life event occurrance");
            });
        });
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            todayBtn: 'linked',
        });
    });
</script>