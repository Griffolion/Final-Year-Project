<div class='panel panel-group' id='formTitle'>
    <h1>New Life Event</h1>
</div>

<div class="panel panel-group" id="formBody">
    <?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
            'id'=>'lifeevent-form',
            'enableClientValidation'=>true,
            'clientOptions' => array(
                'validateOnSubmit'=>true,
                'validateOnChange'=>true,
                'validateOnType'=>true,
            ),
            'htmlOptions' => array('class' => 'well'),
    )); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldGroup($model,'Name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

            <?php echo $form->textFieldGroup($model,'Notes',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

    <div class="form-actions">
            <button class="btn btn-primary" id="newLESub" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i><span>  </span>Save</button>
            <button class="btn btn-default" type="reset"><i class="glyphicon glyphicon-repeat"></i><span>  </span>Reset</button>
    </div>

    <?php $this->endWidget(); ?>
</div>