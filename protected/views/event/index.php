<?php
$this->breadcrumbs=array(
	'Events',
);

Yii::app()->clientScript->registerScript('ajaxlines', "

   function updateChild(id, options)
    {
        try{
            /*  Extract the Primary Key from the CGridView's clicked row */
            console.log(id);
            var pK = parseInt($.fn.yiiGridView.getSelection(id));
            console.log(pK);
            if (pK > -1) {
                //grab data from url 
                var jqxhr = $.ajax( {
                    type: 'POST',
                    url: 'event/updateChild',
                    data: { pk: pK }
                })
                .done(function(data) {
                    $('#updateData').html(data);
                    $('#accordion').collapse();
                    $('#accordion2').collapse();
                })
                .fail(function() {
                   console.log('Could not update child view');
                })
            } 
                
        }
        catch (ex){
            alert(ex.message); /*** Send this to the server for logging when in
                       production ***/
        }
    }
"
);
?>

<h1>Events</h1>

<div class="col-xs-4">
    <button class="btn btn-success" id="create"><span class="glyphicon glyphicon-plus"></span><span> </span><span class="glyphicon glyphicon-calendar"></span><span>  </span>New Event</button>
    <?php
    $this->widget('booster.widgets.TbGridView', array(
        'id' => 'mainEvtGrid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectionChanged' => "updateChild", // new code
        'columns' => array(
            'Name',
            'Start'
        )
    ));
    ?>
    
    
</div>

<div class="col-xs-8" id="updateData">
    
</div>
<script>
$(function(){
        //have to do it like this because its not on the page when it loads
        $('body').on('click','#newESub',function(e){ // Submit button for new
            //prevent the form from submitting
            e.preventDefault();
               var jqxhr = $.ajax( {
                type: 'POST',
                url: 'event/create',
                data: { Event: $('#event-form').serialize() },
                })
            .done(function(data) {
                $('#updateData').html(data);
                $('#accordion').collapse();
                $('#accordion2').collapse();
                $.fn.yiiGridView.update('mainEvtGrid');
                $.notify("Successfully Created!", "success");
            })
            .fail(function() {
                console.log("New event creation failed.");
            });
        });
       $('#create').click(function(){ // Green button to bring new form
          var jqxhr = $.ajax( {
                type: 'POST',
                url: 'event/create',
                data: { create: 1 }
            })
            .done(function(data) {
                $('#updateData').html(data);
                $('#formTitle').collapse();
                $('#formBody').collapse();
            })
            .fail(function() {
               console.log("New event form failed.")
            })
    });
})
</script>