<?php
/* @var $this EventController */
/* @var $Details Event */
?>
<div class="panel panel-group" id="accordion2">
    <h1> 
        <?php echo $Details->Name; 
              /* if (Yii::app()->getModule('user')->isAdmin()) {
                    echo '<button id="dP0" class="btn btn-danger pull-right" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-trash"></i><span>  </span>Delete</button>';
              } */
          ?>
    </h1>
</div>

<div class="modal fade" id="myModal">
  <div class="modal-dialog alert-danger">
    <div class="modal-content alert-danger">
      <div class="modal-header alert-danger">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h1><span class="glyphicon glyphicon-exclamation-sign"></span> Are you sure?</h1>
      </div>
      <div class="modal-body">
          <h4>Deleting this event is irreversibly permanent</h4>
          <h6>Only continue if you are sure you wish to delete this record</h6>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="dP" class="btn btn-danger pull-right" data-dismiss="modal"><i class="glyphicon glyphicon-trash"></i><span>  </span>Delete this event</button>
      </div>
    </div>
  </div>
</div>

<div class="panel panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          Edit Details
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel panel-collapse collapse">
      <div class="panel panel-body">
        <?php
        $this->widget('bootstrap.widgets.TbEditableDetailView', array(
            'data' => $Details,
            'url' => 'event/update',
            'attributes' => array(
                'Name',
                'Start',
                'End'
            ),
        ));
        ?>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
          <?php echo "Ministry"; ?>
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
        <?php
            $this->widget('bootstrap.widgets.TbDetailView', array(
                    'id' => "minGrid",
                    'type' => 'striped bordered',
                    'data' => $Details->getRelated('ministry'),
                    'attributes' => array(
                        'Name',
                        'Description'
                    )
            ));
        ?>
      </div>
    </div>
  </div>
    <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
          <?php echo "Attendance"; ?>
        </a>
      </h4>
    </div>
        <div id="collapseThree" class="panel-collapse collapse">
            <div class="panel-body" id="pplBody">
                <?php
                    if ($relatedData["people"]->getItemCount() < 1) {
                        echo '<div class="alert alert-info" role="alert"><strong>Information: </strong> This family has no members</div>';
                    }
                    else {
                        $this->widget('bootstrap.widgets.TbGridView', array(
                            'id' => "pplGrid",
                            'type' => 'striped bordered',
                            'dataProvider' => $relatedData['people'],
                            'htmlOptions' => array(
                                'style' => 'padding-bottom: 10px'
                            ),
                            'columns' => array(
                                array(
                                    'name' => 'Capacity',
                                    'value' => function($data)use($relatedData){
                                                $criteria = new CDbCriteria();
                                                $criteria->addCondition('Ministry = ' . $relatedData['ministry']->ID . ' AND Person = ' . $data->ID);
                                                $mi = MinistryInvolvement::model()->find($criteria);
                                                if ($mi === NULL) {return 'Attendee';}
                                                else {return $mi->Capacity;}
                                                }
                                ),
                                array(
                                    'name' => 'Name',
                                    'value' => '$data->fullName'
                                ),
                                array(
                                    'name' => 'Address',
                                    'value' => '$data->FullAddress'
                                ),
                                array(
                                    'name' => 'City',
                                    'value' => '$data->City'
                                ),
                                array(
                                    'name' => 'State',
                                    'value' => '$data->State'
                                ),
                                array(
                                    'name' => 'ZIP',
                                    'value' => '$data->ZIP'
                                ),
                                array(
                                    'htmlOptions' => array('nowrap'=>'nowrap'),
                                    'class'=>'bootstrap.widgets.TbButtonColumn',
                                    'buttons' => array(
                                        'view' => array(
                                            'visible' => function($row,$data){false;},
                                        ),
                                        'update' => array(
                                            'visible' => function($data,$row){false;},
                                        ),
                                        'delete' => array(
                                            'url' => function($data) use ($Details){
                                                        return Yii::app()->createUrl('/event/deleteLinkRecord',
                                                        array('thisID'=>$Details->ID, 'linkData'=>$data->ID, 'linkName'=>'Person', 'Entity'=> 'Event')
                                                        );
                                                    },
                                            'visible' => function($data,$row){return Yii::app()->getModule('user')->isAdmin();},
                                        )
                                    )
                            )
                            )
                        ));
                    }
                    if ($relatedData['availablePeople'] !== NULL) {
                        $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
                            'id' => 'newAttendee-form',
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                                'validateOnChange' => true,
                                'validateOnType' => true,
                            ),
                            'htmlOptions' => array('class' => 'well'),
                        )
                        );
                        echo '<div class="form-group">';
                        echo '<h4>Add a new attendee</h4>';
                        $list = CHtml::listData($relatedData['availablePeople'], 'ID', 'fullName');
                        echo CHtml::dropDownList('personDrop', '', $list, array('prompt' => 'Select a Person', 'class' => 'input-large form-control'));
                        echo CHtml::textField('attendedPicker', $Details->Start, array('class' => 'form-control ct-form-control span5',));
                        echo '<div class="form-actions" style="padding-top: 10px">';
                        echo '<button class="btn btn-primary" id="newEAtt" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i><span>  </span>Save</button>';
                        echo '<button class="btn btn-default" type="reset"><i class="glyphicon glyphicon-repeat"></i><span>  </span>Reset</button>';
                        echo '</div>';
                        echo '</div>';
                        $this->endWidget();
                    }
                    else {
                        echo '<div class="alert alert-info" role="alert"><strong>Information: </strong> All available people are attending this event</div>';
                    }
                    
                ?>
            </div>
    </div>
  </div>
</div>

<script>
    $(function(){
        $('#dP').click(function(){
            $('#myModal').on('hidden.bs.modal', function (e) {
                   var pK = <?php echo $Details->ID; ?>;
                   var jqxhr = $.ajax( {
                    type: 'POST',
                    url: 'event/delete',
                    data: { pk: pK },
                    })
                .done(function(data) {
                    $('#updateData').html(data);
                    $('#accordionDel').collapse();
                    $.notify("Successfully Deleted", "success");
                    $.fn.yiiGridView.update('mainEvtGrid');
                })
                .fail(function() {
                    console.log("Deleting the event failed. Check model, controller and database");
                })
            })
        });
        $('body').on('click', '#newEAtt', function(e){
            e.preventDefault();
            var personElement = document.getElementById("personDrop");
            var Select = personElement.options[personElement.selectedIndex].value;
            var selDate = document.getElementById("attendedPicker").value;
            var jqxhr = $.ajax({
                type: 'POST',
                url: 'event/newEAtt',
                data: {Person: Select,
                       Event: <?php echo $Details->ID; ?>,
                       Date: selDate},
                cache: false
            })
            .done(function() {
                $.notify("Added a new attendee", "success");
            })
            .fail(function() {
                console.log("Unable to add a new attendee");
            });
        });
    });
    $('#attendedPicker').datepicker({
        todayBtn: 'linked',
        todayHighlight: true,
        format: 'yyyy-mm-dd',
    });
</script>