<div class='panel panel-group' id='formTitle'>
    <h1>New Contribution</h1>
</div>
    
<div class="panel panel-group" id="formBody">
    <?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
            'id'=>'contribution-form',
            'enableClientValidation'=>true,
            'clientOptions' => array(
                'validateOnSubmit'=>true,
                'validateOnChange'=>true,
                'validateOnType'=>true,
            ),
            'htmlOptions' => array('class' => 'well'),
    )); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

            <?php echo $form->dropDownListGroup($model,'Ministry', array('widgetOptions'=>array('data'=>$ministries, 'htmlOptions'=>array('prompt' => 'Select a Ministry', 'class'=>'input-large')))); ?>

            <?php echo $form->dropDownListGroup($model,'Person', array('widgetOptions'=>array('data'=>$people, 'htmlOptions'=>array('prompt' => 'Select a Person', 'class'=>'input-large')))); ?>

            <?php echo $form->dropDownListGroup($model,'Family', array('widgetOptions'=>array('data'=>$families, 'htmlOptions'=>array('prompt' => 'Select a Family', 'class'=>'input-large')))); ?>

            <?php echo $form->datePickerGroup($model,'Date',array('widgetOptions'=>array('options'=>array('format' => 'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5 datepicker')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click on Month/Year to select a different Month/Year.')); ?>

            <?php echo $form->dropDownListGroup($model,'Type', array('widgetOptions'=>array('data'=>array("Cash"=>"Cash","Check"=>"Check","Regular Bank Transfer"=>"Regular Bank Transfer",), 'htmlOptions'=>array('prompt' => 'Select a Contribution Type', 'class'=>'input-large')))); ?>

            <?php echo $form->numberFieldGroup($model,'Amount',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')),'prepend'=>'<i class="glyphicon glyphicon-usd"></i>')); ?>

            <?php echo $form->textFieldGroup($model,'taxDeductible',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>45)))); ?>

            <?php echo $form->textFieldGroup($model,'checkNumber',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>



    <div class="form-actions">
            <button class="btn btn-primary" id="newCSub" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i><span>  </span>Save</button>
            <button class="btn btn-default" type="reset"><i class="glyphicon glyphicon-repeat"></i><span>  </span>Reset</button>
    </div>

    <?php $this->endWidget(); ?>
</div>
<script>
    $(function(){
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });
    })
</script>