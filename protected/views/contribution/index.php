<?php
$this->breadcrumbs=array(
	'Contributions',
);

Yii::app()->clientScript->registerScript('ajaxlines', "

   function updateChild(id, options)
    {
        try{
            /*  Extract the Primary Key from the CGridView's clicked row */
            console.log(id);
            var pK = parseInt($.fn.yiiGridView.getSelection(id));
            console.log(pK);
            if (pK > -1) {
                //grab data from url 
                var jqxhr = $.ajax( {
                    type: 'POST',
                    url: 'contribution/updateChild',
                    data: { pk: pK }
                })
                .done(function(data) {
                    $('#updateData').html(data);
                    $('#accordion').collapse();
                    $('#accordion2').collapse();
                })
                .fail(function() {
                   console.log('Could not update child view');
                })
            } 
                
        }
        catch (ex){
            alert(ex.message); /*** Send this to the server for logging when in
                       production ***/
        }
    }
    function reinstallDatepicker() {
        $('#CDatePicker').datepicker({
            format: 'yyyy-mm-dd',
            todayBtn: 'linked',
            autoclose: true,
        });
    }
"
);
?>

<h1>Contributions</h1>

<div class="col-xs-4">
    <button class="btn btn-success" id="create"><i class="glyphicon glyphicon-plus"></i><i class="glyphicon glyphicon-usd"></i><span>  </span>New Contribution</button>
    <?php
    $this->widget('booster.widgets.TbGridView', array(
        'id' => 'mainContGrid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectionChanged' => "updateChild", // new code
        'afterAjaxUpdate' => 'reinstallDatepicker',
        'columns' => array(
            'Amount',
            array(
                'name' => 'Date',
                'filter' => $this->widget('bootstrap.widgets.TbDatePicker', array(
                    'name' => 'DatePick',
                    'model' => $model,
                    'attribute' => 'Date',
                    'htmlOptions' => array(
                        'id' => 'CDatePicker',
                    ),
                'options' => array(
                    'showOn' => 'focus',
                    'format' => 'yyyy-mm-dd',
                    'todayBtn' => 'linked',
                    'autoclose' => 'true'
                )
                ), true),
                
            )
        )
    ));
    ?>
    
</div>

<div class="col-xs-8" id="updateData">
    
</div>

<script>
$(function(){
        //have to do it like this because its not on the page when it loads
        $('body').on('click','#newCSub',function(e){ // Submit button for new
            //prevent the form from submitting
            e.preventDefault();
               var jqxhr = $.ajax( {
                type: 'POST',
                url: 'contribution/create',
                data: { Contribution: $('#contribution-form').serialize() },
                })
            .done(function(data) {
                $.notify("Successfully Created!", "success");
                $('#updateData').html(data);
                $.fn.yiiGridView.update('mainContGrid');
                $('#accordion').collapse();
                $('#accordion2').collapse();
            })
            .fail(function() {
               console.log("Failed to create new contribution");
            })
            
        });
       $('#create').click(function(){ // Green button to bring new form
          var jqxhr = $.ajax( {
                type: 'POST',
                url: 'contribution/create',
                data: { create: 1 }
            })
            .done(function(data) {
                $('#updateData').html(data);
                $('#formTitle').collapse();
                $('#formBody').collapse();
            })
            .fail(function() {
               console.log("Failed to open new contribution form")
            })
          
    });
    $('.datepicker').datepicker({
    });
})
</script>