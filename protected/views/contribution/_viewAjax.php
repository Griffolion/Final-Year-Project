<?php
/*  @var $this ContributionController */
/*  @var $Details Contribution */
/*  @var $relatedData array */


?>
<div class="panel panel-group" id="accordion2">
    <h1> 
        <?php echo '$' . $Details->Amount . " from " . $relatedData['person']->fullName;
              /* if (Yii::app()->getModule('user')->isAdmin()) {
                echo '<button id="dP0" class="btn btn-danger pull-right" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-trash"></i><span>  </span>Delete Contribution</button>';
              }*/
        ?>
    </h1>
</div>

<div class="panel panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
              <?php echo "Details"; ?>
            </a>
          </h4>
        </div>
            <div id="collapseOne" class="panel-collapse collapse">
                <div class="panel-body" id="p1">
                    <?php
                    $this->widget('bootstrap.widgets.TbEditableDetailView', array(
                        'data' => $Details,
                        'url' => 'contribution/update',
                        'id' => 'editable',
                        'attributes' => array(
                            'Amount',
                            array(
                                'name' => 'Type',
                                'data' => $Details->Type,
                                'editable' => array(
                                    'type' => 'select',
                                    'source' => array(
                                        'Cash' => 'Cash',
                                        'Check' => 'Check',
                                        'Regular Bank Transfer' => 'Regular Bank Transfer',
                                    )
                                )
                            ),
                            array(
                                'name' => 'Date',
                                'data' => $Details->Date,
                                'editable' => array(
                                    'type' => 'date',
                                    'format' => 'yyyy-mm-dd'
                                    )
                                ),
                            'taxDeductible',
                            'checkNumber'
                        ),
                    ));
                    ?>
                </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
              <?php echo "Person"; ?>
            </a>
          </h4>
        </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $relatedData['person'],
                        'id' => 'personData',
                        'attributes' => array(
                            'fullName',
                            'Address1',
                            'City',
                            'State',
                            'ZIP',
                            'homePhone'
                        ),
                    ));
                    ?>
                </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
              <?php echo "Family"; ?>
            </a>
          </h4>
        </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $relatedData['family'],
                        'id' => 'familyData',
                        'attributes' => array(
                            'Name',
                            'Address1',
                            'Address2',
                            'Address3',
                            'City',
                            'State',
                            'ZIP'
                        )
                    ));
                    ?>
                </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
              <?php echo "Ministry"; ?>
            </a>
          </h4>
        </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $relatedData['ministry'],
                        'id' => 'ministryData',
                        'attributes' => array(
                            'Name',
                            'Description'
                        ),
                    ));
                    ?>
                </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal">
  <div class="modal-dialog alert-danger">
    <div class="modal-content alert-danger">
      <div class="modal-header alert-danger">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h1><span class="glyphicon glyphicon-exclamation-sign"></span> Are you sure?</h1>
      </div>
      <div class="modal-body">
          <h4>Deleting this contribution entry is irreversibly permanent</h4>
          <h6>Only continue if you are sure you wish to delete this record</h6>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="dP" class="btn btn-danger pull-right" data-dismiss="modal"><i class="glyphicon glyphicon-trash"></i><span>  </span>Delete record</button>
      </div>
    </div>
  </div>
</div>

<script>
$(function(){
    $('#dP').click(function(){
            $('#myModal').on('hidden.bs.modal', function (e) {
                   var pK = <?php echo $Details->ID; ?>;
                   var jqxhr = $.ajax( {
                    type: 'POST',
                    url: 'contribution/delete',
                    data: { pk: pK },
                    })
                .done(function(data) {
                    $.fn.yiiGridView.update('mainContGrid');
                    $.notify("Successfully Deleted","success");
                    $('#updateData').html(data);
                    $('#accordionDel').collapse();
                })
                .fail(function() {
                   // alert( 'error' );
                })
            })
    });
})
</script>