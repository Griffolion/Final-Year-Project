<?php
/* @var $this MinistryController */
/* @var $Details Ministry */
/* @var $relatedData array */
?>
<div class="panel panel-group" id="accordion2">
    <h1>
        <?php  echo $Details->Name;
               /* if (Yii::app()->getModule('user')->isAdmin()) {
                echo '<button id="dP0" class="btn btn-danger pull-right" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-trash"></i><span>  </span>Delete ' . $Details->Name . '</button>';
              } */
        ?>
    </h1>
</div>


<div class="modal fade" id="myModal">
  <div class="modal-dialog alert-danger">
    <div class="modal-content alert-danger">
      <div class="modal-header alert-danger">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h1><span class="glyphicon glyphicon-exclamation-sign"></span> Are you sure?</h1>
      </div>
      <div class="modal-body">
          <h4>Deleting this ministry is irreversibly permanent</h4>
          <h6>Only continue if you are sure you wish to delete their record</h6>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="dP" class="btn btn-danger pull-right" data-dismiss="modal"><i class="glyphicon glyphicon-trash"></i><span>  </span>Delete ministry</button>
      </div>
    </div>
  </div>
</div>

<div class="panel panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          Edit Details
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel panel-collapse collapse">
      <div class="panel panel-body">
                <?php
                    $this->widget('bootstrap.widgets.TbEditableDetailView', array(
                    'data' => $Details,
                    'url' => 'ministry/update',
                    'id' => 'editable',
                    'attributes' => array(
                        'Name',
                        'Description',
                    ),
                ));
                ?>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
          <?php echo "Contributions"; ?>
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
        <?php
            if (count($relatedData["contributions"]) < 1)
            {
                echo '<div class="alert alert-info" role="alert"><strong>Information: </strong> No contribution data for this ministry</div>';
            }
            else {
                $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => "contGrid",
                        'type' => 'striped bordered',
                        'dataProvider' => $relatedData['contributions'],
                        'selectableRows' => false,
                        'columns' => array(
                            array(
                                'name' => 'Amount ($)',
                                'value' => '$data->Amount'
                            ),
                            array(
                                'name' => 'Contributer',
                                'value' => function($data){$p = Person::model()->findByPk($data->Person); return $p->fullName;}
                            ),
                            array(
                                'name' => 'Family',
                                'value' => function($data){$f = Family::model()->findByPk($data->Family); return $f->Name;}
                            ),
                            array(
                                'name' => 'Date',
                                'value' => '$data->Date'
                            ),
                        )
                ));
            }
        ?>
      </div>
    </div>
  </div>
    <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
          <?php echo "Events"; ?>
        </a>
      </h4>
    </div>
        <div id="collapseThree" class="panel-collapse collapse">
            <div class="panel-body" id="evtBody">
                <?php
                    if (count($relatedData["events"]) < 1)
                    {
                        echo '<div class="alert alert-info" role="alert"><strong>Information: </strong> This ministry has no events</div>';
                    }
                    else {
                        $this->widget('bootstrap.widgets.TbGridView', array(
                            'id' => "evtGrid",
                            'type' => 'striped bordered',
                            'dataProvider' => $relatedData['events'],
                            'selectableRows' => true,
                            'columns' => array(
                                array(
                                    'name' => 'Name',
                                    'value' => '$data->Name'
                                ),
                                array(
                                    'name' => 'Start Date',
                                    'value' => '$data->Start'
                                ),
                                array(
                                    'name' => 'End Date',
                                    'value' => '$data->End'
                                ),
                            )
                        ));
                    }
                ?>
            </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
          <?php echo "People"; ?>
        </a>
      </h4>
    </div>
        <div id="collapseFour" class="panel-collapse collapse">
            <div class="panel-body" id="pplBody">
                <?php
                    if ($relatedData['people']->getItemCount() < 1)
                    {
                        echo '<div class="alert alert-info" role="alert"><strong>Information: </strong> No people volunteer for this ministry</div>';
                    }
                    else {
                        $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => "pplGrid",
                        'type' => 'striped bordered',
                        'dataProvider' => $relatedData['people'],
                        'selectableRows' => true,
                        'htmlOptions' => array('style' => 'padding-bottom: 10px'),
                        'columns' => array(
                            array(
                                'name' => 'Capacity',
                                'value' => function($data)use($Details){
                                            $criteria = new CDbCriteria();
                                            $criteria->addCondition('Person = ' . $data->ID . ' AND Ministry = ' . $Details->ID);
                                            $mi = MinistryInvolvement::model()->find($criteria);
                                            return $mi->Capacity;
                                            }
                            ),
                            array(
                                'name' => 'Name',
                                'value' => '$data->firstName'
                            ),
                            array(
                                'name' => 'Address',
                                'value' => '$data->Address1'
                            ),
                            array(
                                'htmlOptions' => array('nowrap'=>'nowrap'),
                                'class'=>'bootstrap.widgets.TbButtonColumn',
                                'buttons' => array(
                                    'view' => array(
                                        'visible' => function($row,$data){false;},
                                    ),
                                    'update' => array(
                                        'visible' => function($data,$row){false;},
                                    ),
                                    'delete' => array(
                                        'url' => function($data) use ($Details){
                                                    return Yii::app()->createUrl('/ministry/deleteLinkRecord',
                                                    array('thisID'=>$Details->ID, 'linkData'=>$data->ID, 'linkName'=>'Person', 'Entity'=> 'Ministry')
                                                    );
                                                },
                                        'visible' => function($data,$row){return Yii::app()->getModule('user')->isAdmin();},
                                    )
                                )
                            )
                        )
                    ));
                    }
                    if ($relatedData['availablePeople'] !== NULL) {
                        $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
                        'id' => 'newVolunteer-form',
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => true,
                            'validateOnType' => true,
                        ),
                        'htmlOptions' => array('class' => 'well'),
                            )
                        );
                        echo '<div class="form-group">';
                        echo '<h4>Add a volunteer to ' . $Details->Name . '</h4>';
                        $list = CHtml::listData($relatedData['availablePeople'], 'ID', 'fullName');
                        echo CHtml::dropDownList('personDrop', '', $list, array('prompt' => 'Select a Person', 'class' => 'input-large form-control'));
                        echo CHtml::dropDownList('capacityDrop', '', array('Leader' => 'Leader', 'Volunteer' => 'Volunteer'), array('prompt' => 'Capacity Serving In', 'class' => 'input-large form-control'));
                        echo '<div class="form-actions" style="padding-top: 10px">';
                        echo '<button class="btn btn-primary" id="newPVol" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i><span>  </span>Save</button>';
                        echo '<button class="btn btn-default" type="reset"><i class="glyphicon glyphicon-repeat"></i><span>  </span>Reset</button>';
                        echo '</div>';
                        echo '</div>';
                        $this->endWidget();
                    }
                    else {
                        echo '<div class="alert alert-info" role="alert"><strong>Information: </strong> No more people can be volunteered for this ministry</div>';
                    }
                    
                ?>
            </div>
    </div>
  </div>
</div>
<script>
    $(function(){
        $('#dP').click(function(){
            $('#myModal').on('hidden.bs.modal', function (e) {
                   var pK = <?php echo $Details->ID; ?>;
                   var jqxhr = $.ajax( {
                    type: 'POST',
                    url: 'ministry/delete',
                    data: { pk: pK },
                    })
                .done(function(data) {
                    $('#updateData').html(data);
                    $('#accordionDel').collapse();
                    $.notify("Successfully Deleted", "success");
                    $.fn.yiiGridView.update('minMainGrid');
                })
                .fail(function() {
                    console.log("Deleting the ministry failed. Check model, controller and database");
                })
            })
        });
        $('body').on('click', '#newPVol', function(e){
            e.preventDefault();
            var personElement = document.getElementById("personDrop");
            var Select = personElement.options[personElement.selectedIndex].value;
            var capacityElement = document.getElementById("capacityDrop");
            var selCap = capacityElement.options[capacityElement.selectedIndex].value;
            var jqxhr = $.ajax({
                type: 'POST',
                url: 'ministry/newVol',
                data: {Person: Select,
                       Ministry: <?php echo $Details->ID; ?>,
                       Capacity: selCap},
                cache: false
            })
            .done(function() {
                $.notify("Added " + personElement.options[capacityElement.selectedIndex].key + " as a volunteer", "success");
            })
            .fail(function() {
            });
        });
    });
</script>