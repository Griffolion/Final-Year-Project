<?php
$this->breadcrumbs=array(
	'Ministries',
);

Yii::app()->clientScript->registerScript('ajaxlines', "

   function updateChild(id, options)
    {
        try{
            /*  Extract the Primary Key from the CGridView's clicked row */
            console.log(id);
            var pK = parseInt($.fn.yiiGridView.getSelection(id));
            console.log(pK);
            if (pK > -1) {
                //grab data from url 
                var jqxhr = $.ajax( {
                    type: 'POST',
                    url: 'ministry/updateChild',
                    data: { pk: pK }
                })
                .done(function(data) {
                    $('#updateData').html(data);
                    $('#accordion').collapse();
                    $('#accordion2').collapse();
                })
                .fail(function() {
                   // alert( 'error' );
                })
            } 
                
        }
        catch (ex){
            alert(ex.message); /*** Send this to the server for logging when in
                       production ***/
        }
    }
"
);
?>

<h1>Ministries</h1>

<div class="col-xs-4">
    <button class="btn btn-success" id="create"><span class="glyphicon glyphicon-plus"></span><span class="glyphicon glyphicon-heart"></span> New Ministry</button>
    <?php
    $this->widget('booster.widgets.TbGridView', array(
        'id' => 'minMainGrid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectionChanged' => "updateChild", // new code
        'columns' => array(
            'Name'
        )
    ));
    ?>
    
    
</div>

<div class="col-xs-8" id="updateData">
    
</div>

<script>
$(function(){
        //have to do it like this because its not on the page when it loads
        $('body').on('click','#newMSub',function(e){ // Submit button for new
            //prevent the form from submitting
            e.preventDefault();
               var jqxhr = $.ajax( {
                type: 'POST',
                url: 'ministry/create',
                data: { Ministry: $('#ministry-form').serialize() },
                cache: false
            })
            .done(function(data) {
                $('#updateData').html(data);
                $('#accordion').collapse();
                $('#accordion2').collapse();
                $.fn.yiiGridView.update('minMainGrid');
                $.notify("Successfully Created!", "success");
            })
            .fail(function() {
            });
            
        });
       $('#create').click(function(){ // Green button to bring new form
          var jqxhr = $.ajax( {
                type: 'POST',
                url: 'ministry/create',
                data: { create: 1 }
            })
            .done(function(data) {
                $('#updateData').html(data);
                $('#formTitle').collapse();
                $('#formBody').collapse();
            })
            .fail(function() {
            });
          
    });
});
</script>