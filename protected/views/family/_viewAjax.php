<?php
/* @var $this FamilyController */
/* @var $Details Family */
/* @var $relatedData array */
?>

<div class="panel panel-group" id="accordion2">
    <h1> 
        <?php echo $Details->Name . ' of ' . $Details->Address1; 
              /* if (Yii::app()->getModule('user')->isAdmin()) {
                    echo '<button id="dP0" class="btn btn-danger pull-right" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-trash"></i><span>  </span>Delete</button>';
              } */
          ?>
    </h1>
</div>

<div class="modal fade" id="myModal">
  <div class="modal-dialog alert-danger">
    <div class="modal-content alert-danger">
      <div class="modal-header alert-danger">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h1><span class="glyphicon glyphicon-exclamation-sign"></span> Are you sure?</h1>
      </div>
      <div class="modal-body">
          <h4>Deleting this family record is irreversibly permanent</h4>
          <h6>Only continue if you are sure you wish to delete their record</h6>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="dP" class="btn btn-danger pull-right" data-dismiss="modal"><i class="glyphicon glyphicon-trash"></i><span>  </span>Delete this family</button>
      </div>
    </div>
  </div>
</div>

<div class="panel panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          Edit Details
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel panel-collapse collapse">
      <div class="panel panel-body">
        <?php
            $this->widget('bootstrap.widgets.TbEditableDetailView', array(
                'data' => $Details,
                'url' => 'family/update',
                'attributes' => array(
                    'Name',
                    'Address1',
                    'Address2',
                    'Address3',
                    'City',
                    array(
                        'name' => 'ZIP',
                        'data' => $Details->ZIP,
                        'editable' => array(
                            'type' => 'text',
                        )
                    ),
                    'State'
                ),
            ));
        ?>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
          <?php echo "Contributions"; ?>
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
        <?php
            if ($relatedData["contributions"]->getItemCount() < 1)
            {
                echo '<div class="alert alert-info" role="alert"><strong>Information: </strong> No contribution data for this family</div>';
            }
            else {
                $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => "contGrid",
                        'type' => 'striped bordered',
                        'dataProvider' => $relatedData['contributions'],
                        'selectableRows' => false,
                        'columns' => array(
                            array(
                                'name' => 'Amount',
                                'value' => '$data->Amount'
                            ),
                            array(
                                'name' => 'Contributer',
                                'value' => function($data){$p = Person::model()->findByPk($data->Person); return $p->fullName;}
                            ),
                            array(
                                'name' => 'Ministry',
                                'value' => function($data){$m = Ministry::model()->findByPk($data->Ministry); return $m->Name;}
                            ),
                            array(
                                'name' => 'Date',
                                'value' => '$data->Date'
                            ),
                        )
                ));
            }
        ?>
      </div>
    </div>
  </div>
    <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
          <?php echo "Family Members"; ?>
        </a>
      </h4>
    </div>
        <div id="collapseThree" class="panel-collapse collapse">
            <div class="panel-body" id="pplBody">
                <?php
                    if ($relatedData["people"]->getItemCount() < 1) {
                        echo '<div class="alert alert-info" role="alert"><strong>Information: </strong> This family has no members</div>';
                    }
                    else {
                        $this->widget('bootstrap.widgets.TbGridView', array(
                            'id' => "pplGrid",
                            'type' => 'striped bordered',
                            'dataProvider' => $relatedData['people'],
                            'selectableRows' => true,
                            'htmlOptions' => array(
                                'style' => 'padding-bottom: 10px'
                            ),
                            'columns' => array(
                                array(
                                    'name' => 'Position',
                                    'value' => function($data)use($Details){
                                                $criteria = new CDbCriteria();
                                                $criteria->addCondition('Family = ' . $Details->ID . ' AND Person = ' . $data->ID);
                                                $fm = FamilyMembership::model()->find($criteria);
                                                return $fm->Position;}
                                ),
                                array(
                                    'name' => 'Name',
                                    'value' => '$data->fullName'
                                ),
                                array(
                                    'name' => 'DOB',
                                    'value' => '$data->DOB'
                                ),
                                array(
                                    'name' => 'Address',
                                    'value' => '$data->FullAddress'
                                ),
                                array(
                                    'name' => 'City',
                                    'value' => '$data->City'
                                ),
                                array(
                                    'name' => 'State',
                                    'value' => '$data->State'
                                ),
                                array(
                                    'name' => 'ZIP',
                                    'value' => '$data->ZIP'
                                ),
                                array(
                                    'htmlOptions' => array('nowrap'=>'nowrap'),
                                    'class'=>'bootstrap.widgets.TbButtonColumn',
                                    'buttons' => array(
                                        'view' => array(
                                            'visible' => function($row,$data){false;},
                                        ),
                                        'update' => array(
                                            'visible' => function($data,$row){false;},
                                        ),
                                        'delete' => array(
                                            'url' => function($data) use ($Details){
                                                        return Yii::app()->createUrl('/family/deleteLinkRecord',
                                                        array('thisID'=>$Details->ID, 'linkData'=>$data->ID, 'linkName'=>'Person', 'Entity'=> 'Family')
                                                        );
                                                    },
                                            'visible' => function($data,$row){return Yii::app()->getModule('user')->isAdmin();},
                                        )
                                    )
                            )
                            )
                        ));
                    }
                    if ($relatedData['availablePeople'] !== NULL) {
                        $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
                            'id' => 'newMember-form',
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                                'validateOnChange' => true,
                                'validateOnType' => true,
                            ),
                            'htmlOptions' => array('class' => 'well'),
                        )
                        );
                        echo '<div class="form-group">';
                        echo '<h4>Add a new family member</h4>';
                        $list = CHtml::listData($relatedData['availablePeople'], 'ID', 'fullName');
                        echo CHtml::dropDownList('personDrop', '', $list, array('prompt' => 'Select a Person', 'class' => 'input-large form-control'));
                        echo CHtml::dropDownList('positionDrop', '', array('Parent' => 'Parent', 'Child' => 'Child'), array('prompt' => 'Parent or Child', 'class' => 'input-large form-control'));
                        echo '<div class="form-actions" style="padding-top: 10px">';
                        echo '<button class="btn btn-primary" id="newFMem" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i><span>  </span>Save</button>';
                        echo '<button class="btn btn-default" type="reset"><i class="glyphicon glyphicon-repeat"></i><span>  </span>Reset</button>';
                        echo '</div>';
                        echo '</div>';
                        $this->endWidget();
                    }
                    else {
                        echo '<div class="alert alert-info" role="alert"><strong>Information: </strong> All available people are in this family</div>';
                    }
                ?>
            </div>
    </div>
  </div>
</div>


<script>
    $(function(){
        $('#dP').click(function(){
            $('#myModal').on('hidden.bs.modal', function (e) {
                   var pK = <?php echo $Details->ID; ?>;
                   var jqxhr = $.ajax( {
                    type: 'POST',
                    url: 'family/delete',
                    data: { pk: pK },
                    })
                .done(function(data) {
                    $('#updateData').html(data);
                    $('#accordionDel').collapse();
                    $.notify("Successfully Deleted", "success");
                    $.fn.yiiGridView.update('mainFamGrid');
                })
                .fail(function() {
                    console.log("Deleting the family failed. Check model, controller and database");
                })
            })
        });
        $('body').on('click', '#newFMem', function(e){
            e.preventDefault();
            var personElement = document.getElementById("personDrop");
            var Select = personElement.options[personElement.selectedIndex].value;
            var capacityElement = document.getElementById("positionDrop");
            var selPos = capacityElement.options[capacityElement.selectedIndex].value;
            var jqxhr = $.ajax({
                type: 'POST',
                url: 'family/newMem',
                data: {Person: Select,
                       Family: <?php echo $Details->ID; ?>,
                       Position: selPos},
                cache: false
            })
            .done(function() {
                $.notify("Added a new family member", "success");
            })
            .fail(function() {
                console.log("Unable to add a new family member");
            });
        });
    });
</script>