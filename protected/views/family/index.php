<?php
$this->breadcrumbs=array(
	'Families',
);

Yii::app()->clientScript->registerScript('ajaxlines', "

   function updateChild(id, options)
    {
        try{
            /*  Extract the Primary Key from the CGridView's clicked row */
            console.log(id);
            var pK = parseInt($.fn.yiiGridView.getSelection(id));
            console.log(pK);
            if (pK > -1) {
                //grab data from url 
                var jqxhr = $.ajax( {
                    type: 'POST',
                    url: 'family/updateChild',
                    data: { pk: pK }
                })
                .done(function(data) {
                    $('#updateData').html(data);
                    $('#accordion').collapse();
                    $('#accordion2').collapse();
                })
                .fail(function() {
                   console.log('updateChild failed.');
                })
            } 
                
        }
        catch (ex){
            alert(ex.message); /*** Send this to the server for logging when in
                       production ***/
        }
    }
"
);
?>
<h1>Families</h1>

<div class="col-xs-4">
    <button class="btn btn-success" id="create"><i class="glyphicon glyphicon-plus"></i><i class="glyphicon glyphicon-home"></i><span>  </span>New Family</button>
    <?php
    $this->widget('booster.widgets.TbGridView', array(
        'id' => 'mainFamGrid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectionChanged' => "updateChild", // new code
        'columns' => array(
            'Name',
            'Address1'
        )
    ));
    ?>
</div>

<div class="col-xs-8" id="updateData">
    
</div>

<script>
$(function(){
        //have to do it like this because its not on the page when it loads
        $('body').on('click','#newFSub',function(e){ // Submit button for new
            //prevent the form from submitting
            e.preventDefault();
               var jqxhr = $.ajax( {
                type: 'POST',
                url: 'family/create',
                data: { Family: $('#family-form').serialize() },
                cache: false
            })
            .done(function(data) {
                $('#updateData').html(data);
                $('#accordion').collapse();
                $('#accordion2').collapse();
                $.fn.yiiGridView.update('mainFamGrid');
                $.notify("Successfully Created!", "success");
            })
            .fail(function() {
                console.log("New family creation failed.");
            });
            
        });
       $('#create').click(function(){ // Green button to bring new form
          var jqxhr = $.ajax( {
                type: 'POST',
                url: 'family/create',
                data: { create: 1 }
            })
            .done(function(data) {
                $('#updateData').html(data);
                $('#formTitle').collapse();
                $('#formBody').collapse();
            })
            .fail(function() {
                console.log("New family form failed.");
            });
          
    });
});
</script>