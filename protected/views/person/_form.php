<div class='panel panel-group' id='formTitle'>
    <h1>New Person</h1>
</div>
<div class="panel panel-group" id="accordion">
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'person-form',
        'enableClientValidation'=>true,
        'clientOptions' => array(
            'validateOnSubmit'=>true,
            'validateOnChange'=>true,
            'validateOnType'=>true,
        ),
        'htmlOptions' => array('class' => 'well'),
)); 
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListGroup($model,'Title', array('widgetOptions'=>array('data'=>array("Mr"=>"Mr","Mrs"=>"Mrs","Miss"=>"Miss","Ms"=>"Ms","Dr"=>"Dr",), 'htmlOptions'=>array('class'=>'input-large')))); ?>
        
	<?php echo $form->textFieldGroup($model,'firstName',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'middleName',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'lastName',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->datePickerGroup($model,'DOB',array('widgetOptions'=>array('options'=>array('format' => 'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click on Month/Year to select a different Month/Year.')); ?>

	<?php echo $form->textFieldGroup($model,'Address1',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'Address2',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'Address3',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'City',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'ZIP',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10)))); ?>

	<?php echo $form->textFieldGroup($model,'State',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>2)))); ?>

	<?php echo $form->textFieldGroup($model,'Occupation',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'homePhone',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10)))); ?>

	<?php echo $form->textFieldGroup($model,'cellPhone',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10)))); ?>

	<?php echo $form->textFieldGroup($model,'workPhone',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10)))); ?>

	<?php echo $form->textFieldGroup($model,'homeEmail',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'workEmail',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->dropDownListGroup($model,'memberStatus', array('widgetOptions'=>array('data'=>array("Attending"=>"Attending","Attending Regularly"=>"Attending Regularly","Church Member"=>"Church Member","Conference Member"=>"Conference Member","Shut-in"=>"Shut-in",), 'htmlOptions'=>array('class'=>'input-large')))); ?>

	<?php echo $form->datePickerGroup($model,'dateJoined',array('widgetOptions'=>array('options'=>array('format' => 'yyyy-mm-dd', 'viewFormat' => 'mm-dd-yyyy'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click on Month/Year to select a different Month/Year.')); ?>

	<?php echo $form->dropDownListGroup($model,'Gender', array('widgetOptions'=>array('data'=>array("Female"=>"Female","Male"=>"Male",), 'htmlOptions'=>array('class'=>'input-large')))); ?>

	<?php echo $form->dropDownListGroup($model,'maritalStatus', array('widgetOptions'=>array('data'=>array("Engaged"=>"Engaged","In a Relationship"=>"In a Relationship","Married"=>"Married","Single"=>"Single",), 'htmlOptions'=>array('class'=>'input-large')))); ?>

	<?php echo $form->textAreaGroup($model,'Notes', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>

	<?php echo $form->dropDownListGroup($model,'Active', array('widgetOptions'=>array('data'=>array("Active"=>"Active","Inactive"=>"Inactive",), 'htmlOptions'=>array('class'=>'input-large')))); ?>

<div class="form-actions">
	<button class="btn btn-primary" id="newPSub" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i><span>  </span>Save</button>
        <button class="btn btn-default" type="reset"><i class="glyphicon glyphicon-repeat"></i><span>  </span>Reset</button>
</div>

<?php $this->endWidget(); ?>
</div>