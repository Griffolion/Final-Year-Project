<?php
$this->breadcrumbs = array(
    'People',
);
Yii::app()->clientScript->registerScript('ajaxlines', "

   function updateChild(id, options)
    {
        try{
            /*  Extract the Primary Key from the CGridView's clicked row */
            console.log(id);
           // alert($.fn.yiiGridView.getSelection(id));
            var pK = parseInt($.fn.yiiGridView.getSelection(id));
            console.log(pK);
            if (pK > -1) {
                //grab data from url 
                var jqxhr = $.ajax( {
                    type: 'POST',
                    url: 'person/updateChild',
                    data: { pk: pK },
                    cache: false,
                })
                .done(function(data) {
                    $('#updateData').html(data);
                    $('#accordion').collapse();
                    $('#accordion2').collapse();
                })
                .fail(function() {
                })
            } 
                
        }
        catch (ex){
            alert(ex.message); /*** Send this to the server for logging when in
                       production ***/
        }
    }
"
);
?>

<script>
$(function(){
    $('body').on('click','#newPSub',function(e){ // Submit button for new
        //prevent the form from submitting
        e.preventDefault();
        var jqxhr = $.ajax( {
            type: 'POST',
            url: 'person/create',
            data: { Person: $('#person-form').serialize() },
            cache: false
        })
        .done(function(data) {
            $('#updateData').html(data);
            $('#accordion').collapse();
            $('#accordion2').collapse();
            $.fn.yiiGridView.update('mainGrid');
            $.notify("Successfully Created!", "success");
        })
        .fail(function() {
        });
    });
   $('#create').click(function(){ // Green button to bring new form
      var jqxhr = $.ajax( {
            type: 'POST',
            url: 'person/create',
            data: { create: 1 }
        })
        .done(function(data) {
            $('#updateData').html(data);
            $('#formTitle').collapse();
            $('#accordion').collapse();
        })
        .fail(function() {
        });

    });
});
</script>

<h1>People</h1>
<div class="col-xs-4" id="parentView">
    <button class="btn btn-success" id="create"><i class="glyphicon glyphicon-plus small"></i><i class="glyphicon glyphicon-user"></i><span>  </span>New Person</button>
    <?php
        $this->widget('bootstrap.widgets.TbGridView', array(
            'id' => 'mainGrid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'selectionChanged' => "updateChild", // new code
            'columns' => array(
                'firstName', 'lastName'
            )
        ));
    ?>
</div>

<div class="col-xs-8" id="updateData">
    
</div>