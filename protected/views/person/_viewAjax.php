<?php
/* @var $this PersonController */
/* @var $Details Person */
/* @var $contributions array() */
/* @var $families CArrayDataProvider */
/* @var $familyList CArrayListProvider */
//Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/highcharts/highcharts.js');
//Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/highcharts/exporting.js');
?>

<div class="panel panel-group" id="accordion2">
<div>
    <h1> 
        <?php
        echo $Details->fullName;
        if (Yii::app()->getModule('user')->isAdmin() && $Details->Active != "Inactive") {
            echo '<button id="dP0" class="btn btn-danger pull-right" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-trash"></i><span>  </span>Deactivate ' . $Details->firstName . '</button>';  
        }
        if ($Details->Active == 'Inactive') {
            echo '<button id="dP00" class="btn btn-danger pull-right disabled">Inactive</button>';
        }
        ?>
    </h1>
</div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog alert-danger">
        <div class="modal-content alert-danger">
            <div class="modal-header alert-danger">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h1><span class="glyphicon glyphicon-exclamation-sign"></span> Are you sure?</h1>
            </div>
            <div class="modal-body">
                <h4>Are you sure you wish to deactivate <?php echo $Details->firstName; ?>?</h4>
                <h6>The action is reversible, but only continue if you are sure</h6>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" id="dP" class="btn btn-danger pull-right" data-dismiss="modal"><i class="glyphicon glyphicon-trash"></i><span>  </span>Delete <?php echo $Details->firstName; ?></button>
            </div>
        </div>
    </div>
</div>


<div class="panel panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    <?php echo "Edit " . $Details->firstName . "'s Details"; ?>
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel panel-collapse collapse">
            <div class="panel panel-body">
                <?php
                $this->widget('bootstrap.widgets.TbEditableDetailView', array(
                    'data' => $Details,
                    'url' => 'person/update',
                    'id' => 'editable',
                    'attributes' => array(
                        array(
                            'name' => 'ID',
                            'data' => $Details->ID,
                            'editable' => false
                        ),
                        array(
                            'name' => 'Title',
                            'data' => $Details->Title,
                            'editable' => array(
                                'type' => 'select',
                                'source' => array(
                                    'Mr' => 'Mr',
                                    'Mrs' => 'Mrs',
                                    'Miss' => 'Miss',
                                    'Ms' => 'Ms',
                                    'Dr' => 'Dr',
                                )
                            )
                        ),
                        'firstName',
                        'middleName',
                        'lastName',
                        array(
                            'name' => 'DOB',
                            'data' => $Details->DOB,
                            'editable' => array(
                                'type' => 'date',
                                'viewformat' => 'mm/dd/yyyy'
                            )
                        ),
                        'Address1',
                        'Address2',
                        'Address3',
                        'City',
                        array(
                            'name' => 'ZIP',
                            'data' => $Details->ZIP,
                            'editable' => array(
                                'type' => 'text',
                            )
                        ),
                        'State',
                        'Occupation',
                        'homePhone',
                        'cellPhone',
                        'workPhone',
                        'homeEmail',
                        'workEmail',
                        array(
                            'name' => 'memberStatus',
                            'data' => $Details->memberStatus,
                            'editable' => array(
                                'type' => 'select',
                                'source' => array(
                                    'Attending' => 'Attending',
                                    'Attending Regularly' => 'Attending Regularly',
                                    'Church Member' => 'Church Member',
                                    'Conference Member' => 'Conference Member',
                                    'Shut-in' => 'Shut-in'
                                )
                            )
                        ),
                        array(
                            'name' => 'Active',
                            'data' => $Details->Active,
                            'editable' => array(
                                'type' => 'select',
                                'source' => array(
                                    'Active' => 'Active',
                                    'Inactive' => 'Inactive'
                                )
                            )
                        ),
                        array(
                            'name' => 'dateJoined',
                            'data' => $Details->dateJoined,
                            'editable' => array(
                                'type' => 'date',
                                'viewformat' => 'mm/dd/yyyy'
                            )
                        ),
                        array(
                            'name' => 'Gender',
                            'data' => $Details->Gender,
                            'editable' => array(
                                'type' => 'select',
                                'source' => array(
                                    'Female' => 'Female',
                                    'Male' => 'Male'
                                )
                            )
                        ),
                        array(
                            'name' => 'maritalStatus',
                            'data' => $Details->maritalStatus,
                            'editable' => array(
                                'type' => 'select',
                                'source' => array(
                                    'Engaged' => 'Engaged',
                                    'In a Relationship' => 'In a Relationship',
                                    'Married' => 'Married',
                                    'Single' => 'Single'
                                )
                            )
                        ),
                        'Notes',
                        array(
                            'name' => 'dateCreated',
                            'data' => $Details->dateCreated,
                            'editable' => false
                        ),
                        array(
                            'name' => 'dateModified',
                            'data' => $Details->dateModified,
                            'editable' => false
                        )
                    ),
                ));
                ?>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    <?php echo "Families"; ?>
                </a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse">
            <div class="panel-body">
                <?php
                if ($relatedData['families']->getItemCount() > 0) {
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => "famGrid",
                        'type' => 'striped bordered',
                        'dataProvider' => $relatedData['families'],
                        'selectableRows' => false,
                        'htmlOptions' => array('style' => 'padding-bottom: 10px'),
                        'columns' => array(
                            array(
                                'name' => 'Position',
                                'value' => function($data)use($Details){
                                                $criteria = new CDbCriteria();
                                                $criteria->addCondition('Person = ' . $Details->ID . ' AND Family = ' . $data->ID);
                                                $fm = FamilyMembership::model()->find($criteria);
                                                return $fm->Position;}
                            ),
                            array(
                                'name' => 'Family Name',
                                'value' => '$data->Name'
                            ),
                            array(
                                'name' => 'First Line Address',
                                'value' => '$data->Address1'
                            ),
                            array(
                                'name' => 'City',
                                'value' => '$data->City'
                            ),
                            array(
                                'name' => 'State',
                                'value' => '$data->State'
                            ),
                            array(
                                'name' => 'ZIP',
                                'value' => '$data->ZIP'
                            ),
                            array(
                                'htmlOptions' => array('nowrap'=>'nowrap'),
                                'class'=>'bootstrap.widgets.TbButtonColumn',
                                'buttons' => array(
                                    'view' => array(
                                        'visible' => function($row,$data){false;},
                                    ),
                                    'update' => array(
                                        'visible' => function($data,$row){false;},
                                    ),
                                    'delete' => array(
                                        'url' => function($data) use ($Details){
                                                    return Yii::app()->createUrl('/person/deleteLinkRecord',
                                                    array('thisID'=>$Details->ID, 'linkData'=>$data->ID, 'linkName'=>'Family', 'Entity'=> 'Family')
                                                    );
                                                },
                                    )
                                )
                            )
                        )
                    ));
                } else {
                    echo '<div class="alert alert-info" role="alert"><strong>Information: </strong>' . $Details->firstName . ' is not in a family yet</div>';
                }
                if ($relatedData['availableFamilies'] !== NULL) {
                    $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
                        'id' => 'familyMember-form',
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => true,
                            'validateOnType' => true,
                        ),
                        'htmlOptions' => array('class' => 'well'),
                            )
                    );
                    echo '<div class="form-group">';
                    echo '<h4>Put ' . $Details->firstName . ' into a new family</h4>';
                    $list = CHtml::listData($relatedData['availableFamilies'], 'ID', 'Name');
                    echo CHtml::dropDownList('familyDrop', '', $list, array('prompt' => 'Select a Family', 'class' => 'input-large form-control'));
                    echo '</div>';
                    echo '<div class="form-actions">';
                    echo '<button class="btn btn-primary" id="newFMSub" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i><span>  </span>Save</button>';
                    echo '<button class="btn btn-default" type="reset"><i class="glyphicon glyphicon-repeat"></i><span>  </span>Reset</button>';
                    echo '</div>';

                    $this->endWidget();
                } else {
                    echo '<div class="alert alert-info" role="alert"><strong>Information: </strong>' . $Details->firstName . ' belongs to all available families</div>';
                }
                ?>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                    <?php echo "Events"; ?>
                </a>
            </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse">
            <div class="panel-body" id="p4">
                <?php
                if ($relatedData['events']->getItemCount() > 0) {
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => "evtGrid",
                        'type' => 'striped bordered',
                        'dataProvider' => $relatedData['events'],
                        'selectableRows' => false,
                        'htmlOptions' => array('style' => 'padding-bottom: 10px'),
                        'columns' => array(
                            array(
                                'name' => 'Event Name',
                                'value' => '$data->Name'
                            ),
                            array(
                                'name' => 'Start Date',
                                'value' => '$data->Start'
                            ),
                            array(
                                'name' => 'End Date',
                                'value' => '$data->End'
                            ),
                            array(
                                'htmlOptions' => array('nowrap'=>'nowrap'),
                                'class'=>'bootstrap.widgets.TbButtonColumn',
                                'buttons' => array(
                                    'view' => array(
                                        'visible' => function($row,$data){false;},
                                    ),
                                    'update' => array(
                                        'visible' => function($data,$row){false;},
                                    ),
                                    'delete' => array(
                                        'url' => function($data) use ($Details){
                                                    return Yii::app()->createUrl('/person/deleteLinkRecord',
                                                    array('thisID'=>$Details->ID, 'linkData'=>$data->ID, 'linkName'=>'Event', 'Entity'=> 'Event')
                                                    );
                                                },
                                    )
                                )
                            )
                        )
                    ));
                } else {
                    echo '<div class="alert alert-info" role="alert"><strong>Information: </strong>' . $Details->firstName . ' has attended no events yet</div>';
                }
                if ($relatedData['availableEvents'] !== NULL) {
                    $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
                        'id' => 'eventAttendance-form',
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => true,
                            'validateOnType' => true,
                        ),
                        'htmlOptions' => array('class' => 'well'),
                            )
                    );
                    echo '<div class="form-group">';
                    echo '<h4>Put ' . $Details->firstName . ' into an Event</h4>';
                    $list = CHtml::listData($relatedData['availableEvents'], 'ID', 'Name');
                    echo CHtml::dropDownList('eventDrop', '', $list, array('prompt' => 'Select an Event', 'class' => 'input-large form-control'));
                    echo $form->datePickerGroup(EventAttendance::model(),'dateAttended',array('widgetOptions'=>array('options'=>array('format' => 'yyyy-mm-dd', 'viewFormat' => 'mm-dd-yyyy'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click on Month/Year to select a different Month/Year.'));
                    echo '</div>';
                    echo '<div class="form-actions">';
                    echo '<button class="btn btn-primary" id="newEASub" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i><span>  </span>Save</button>';
                    echo '<button class="btn btn-default" type="reset"><i class="glyphicon glyphicon-repeat"></i><span>  </span>Reset</button>';
                    echo '</div>';

                    $this->endWidget();
                } else {
                    echo '<div class="alert alert-info" role="alert"><strong>Information: </strong>' . $Details->firstName . ' has attended all available events</div>';
                }
                ?>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                    <?php echo "Contributions"; ?>
                </a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse">
            <div class="panel-body" id="contCol">
                <?php
                if (count($relatedData['contributions']) <= 0) {
                    echo '<div class="alert alert-info" role="alert"><strong>Information: </strong>' . $Details->firstName . ' has no contribution data</div>';
                }
                ?>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                    <?php echo "Ministries"; ?>
                </a>
            </h4>
        </div>
        <div id="collapseFive" class="panel-collapse collapse">
            <div class="panel-body" id="minContainer">
                <?php
                if ($relatedData['ministries']->getItemCount() > 0) {
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => "minGrid",
                        'type' => 'striped bordered',
                        'dataProvider' => $relatedData['ministries'],
                        'selectableRows' => true,
                        'htmlOptions' => array('style' => 'padding-bottom: 10px'),
                        'columns' => array(
                            array(
                                'name' => 'Ministry Name',
                                'value' => '$data->Name'
                            ),
                            array(
                                'name' => 'Description',
                                'value' => '$data->Description'
                            ),
                            array(
                                'htmlOptions' => array('nowrap'=>'nowrap'),
                                'class'=>'bootstrap.widgets.TbButtonColumn',
                                'buttons' => array(
                                    'view' => array(
                                        'visible' => function($row,$data){false;},
                                    ),
                                    'update' => array(
                                        'visible' => function($data,$row){false;},
                                    ),
                                    'delete' => array(
                                        'url' => function($data) use ($Details){
                                                    return Yii::app()->createUrl('/person/deleteLinkRecord',
                                                    array('thisID'=>$Details->ID, 'linkData'=>$data->ID, 'linkName'=>'Ministry', 'Entity'=> 'Ministry')
                                                    );
                                                },
                                    )
                                )
                            )
                        )
                    ));
                } else {
                    echo '<div class="alert alert-info" role="alert"><strong>Information: </strong>' . $Details->firstName . ' is not involved in any ministries</div>';
                }
                if ($relatedData['availableMinistries'] !== NULL) {
                    $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
                        'id' => 'ministryInvolvement-form',
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => true,
                            'validateOnType' => true,
                        ),
                        'htmlOptions' => array('class' => 'well'),
                            )
                    );
                    echo '<div class="form-group">';
                    echo '<h4>Quick-add ' . $Details->firstName . ' into a ministry</h4>';
                    $list = CHtml::listData($relatedData['availableMinistries'], 'ID', 'Name');
                    echo CHtml::dropDownList('ministryDrop', '', $list, array('prompt' => 'Select a Ministry', 'class' => 'input-large form-control'));
                    echo CHtml::dropDownList('capacityDrop', '', array('Leader' => 'Leader', 'Volunteer' => 'Volunteer'), array('prompt' => 'Capacity', 'class' => 'input-large form-control'));
                    echo '</div>';
                    echo '<div class="form-actions">';
                    echo '<button class="btn btn-primary" id="newMISub" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i><span>  </span>Save</button>';
                    echo '<button class="btn btn-default" type="reset"><i class="glyphicon glyphicon-repeat"></i><span>  </span>Reset</button>';
                    echo '</div>';

                    $this->endWidget();
                } else {
                    echo '<div class="alert alert-info" role="alert"><strong>Information: </strong>' . $Details->firstName . ' has volunteered for all available ministries</div>';
                }
                ?>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                    <?php echo "Life Events"; ?>
                </a>
            </h4>
        </div>
        <div id="collapseSix" class="panel-collapse collapse">
            <div class="panel-body" id="p4">
                <?php
                if ($relatedData['lifeEvents']->getItemCount() > 0) {
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => "LEGrid",
                        'type' => 'striped bordered',
                        'dataProvider' => $relatedData['lifeEvents'],
                        'selectableRows' => false,
                        'htmlOptions' => array('style' => 'padding-bottom: 10px'),
                        'columns' => array(
                            array(
                                'name' => 'Life Event Name',
                                'value' => '$data->Name'
                            ),
                            array(
                                'name' => 'Description',
                                'value' => '$data->Notes'
                            ),
                            array(
                                'htmlOptions' => array('nowrap'=>'nowrap'),
                                'class'=>'bootstrap.widgets.TbButtonColumn',
                                'buttons' => array(
                                    'view' => array(
                                        'visible' => function($row,$data){false;},
                                    ),
                                    'update' => array(
                                        'visible' => function($data,$row){false;},
                                    ),
                                    'delete' => array(
                                        'url' => function($data) use ($Details){
                                                    return Yii::app()->createUrl('/person/deleteLinkRecord',
                                                    array('thisID'=>$Details->ID, 'linkData'=>$data->ID, 'linkName'=>'LifeEvent', 'Entity'=> 'LifeEvent')
                                                    );
                                                },
                                    )
                                )
                            )
                        )
                    ));
                } else {
                    echo '<div class="alert alert-info" role="alert"><strong>Information: </strong>' . $Details->firstName . ' has not had any life events</div>';
                }
                if ($relatedData['availableLifeEvents'] !== NULL) {
                    $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
                        'id' => 'lifeEvent-form',
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => true,
                            'validateOnType' => true,
                        ),
                        'htmlOptions' => array('class' => 'well'),
                            )
                    );
                    echo '<div class="form-group">';
                    echo '<h4>Give ' . $Details->firstName . ' a new Life Event</h4>';
                    $list = CHtml::listData($relatedData['availableLifeEvents'], 'ID', 'Name');
                    echo CHtml::dropDownList('lifeEventDrop', '', $list, array('prompt' => 'Select a Life Event', 'class' => 'input-large form-control'));
                    echo '</div>';
                    echo '<div class="form-actions">';
                    echo '<button class="btn btn-primary" id="newLESub" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i><span>  </span>Save</button>';
                    echo '<button class="btn btn-default" type="reset"><i class="glyphicon glyphicon-repeat"></i><span>  </span>Reset</button>';
                    echo '</div>';

                    $this->endWidget();
                } else {
                    echo '<div class="alert alert-info" role="alert"><strong>Information: </strong> There are no available life events for ' . $Details->firstName . '</div>';
                }
                ?>
            </div>
        </div>
    </div>
</div>


<script>
//    $(function() {
//        var keys = [];
//    <?php
        //echo "var contributions = " . json_encode(array_values($relatedData['contributions'])) . ";\n";
        //echo "var contributions2 = " . json_encode($relatedData['contributions']) . ";\n";
    ?>//
//        for (var key in contributions2) {
//            keys.push(key);
//        }
//        if (contributions.length > 0)
//        {
//            var h = new Highcharts.Chart({
//                chart: {
//                    //alignTicks: false,
//                    type: 'line',
//                    renderTo: "contCol",
//                },
//                title: {
//                    text: "<?php //echo $Details->firstName; ?>'s Giving"
//                },
//                xAxis: {
//                    categories: keys
//                },
//                yAxis: [{
//                        title: {
//                            text: 'Amount ($)'
//                        },
//                        gridLineWidth: 0
//                    }, ],
//                legend: {
//                    layout: 'vertical',
//                    backgroundColor: '#FFFFFF',
//                    floating: true,
//                    align: 'center',
//                    x: 100,
//                    verticalAlign: 'top',
//                    y: 70
//                },
//                tooltip: {
//                    formatter: function() {
//                        return '<b>' + this.series.name + '</b><br/>' +
//                                '$' + this.y;
//                    }
//                },
//                plotOptions: {
//                },
//                series: [{
//                        name: '<?php //echo $Details->firstName; ?>',
//                        data: [contributions[0], contributions[1], contributions[2]]
//
//                    }, ]
//            });
//        }
//    });
    $(function() {
        $('#dP').click(function() {
            $('#myModal').on('hidden.bs.modal', function(e) {
                var pK = <?php echo $Details->ID; ?>;
                var jqxhr = $.ajax({
                    type: 'POST',
                    url: 'person/delete',
                    data: {pk: pK},
                })
                .done(function(data) {
                    $('#updateData').html(data);
                    $('#accordionDel').collapse();
                    $.notify("Successfully Deleted", "success");
                    $.fn.yiiGridView.update('mainGrid');
                })
                .fail(function() {
                    console.log("Deleting the ministry failed. Check model, controller and database")
                })
            })
        });
        $('body').on('click', '#newFMSub', function(e) { // Submit button for new
            //prevent the form from submitting
            e.preventDefault();
            var Fam = document.getElementById("familyDrop");
            var Select = Fam.options[Fam.selectedIndex].value;
            var jqxhr = $.ajax({
                type: 'POST',
                url: 'person/newFM',
                data: {Family: Select,
                       Person: <?php echo $Details->ID; ?>},
            })
            .done(function() {
                $.notify("Added to family", "success");
                $.fn.yiiGridView.update('famGrid');
            })
            .fail(function() {
            })
        });
        $('body').on('click', '#newEASub', function(e) { // Submit button for new
            //prevent the form from submitting
            e.preventDefault();
            var Evt = document.getElementById("eventDrop");
            var Select = Evt.options[Evt.selectedIndex].value;
            var jqxhr = $.ajax({
                type: 'POST',
                url: 'person/newEA',
                data: {Event: Select,
                    Person: <?php echo $Details->ID; ?>},
            })
            .done(function() {
                $.notify("Added to event", "success");
                $.fn.yiiGridView.update('evtGrid');
            })
            .fail(function() {
            })
        });
        $('body').on('click', '#newLESub', function(e) { // Submit button for new
            //prevent the form from submitting
            e.preventDefault();
            var LE = document.getElementById("lifeEventDrop");
            var Select = LE.options[LE.selectedIndex].value;
            var jqxhr = $.ajax({
                type: 'POST',
                url: 'person/newLE',
                data: {lifeEvent: Select,
                    Person: <?php echo $Details->ID; ?>},
            })
            .done(function() {
                $.notify("Added a life event", "success");
                $.fn.yiiGridView.update('LEGrid');
            })
            .fail(function() {
            })
        });
        $('body').on('click', '#newMISub', function(e) { // Submit button for new
            //prevent the form from submitting
            e.preventDefault();
            var Min = document.getElementById("ministryDrop");
            var Select = Min.options[Min.selectedIndex].value;
            Min = document.getElementById("capacityDrop");
            var Capacity = Min.options[Min.selectedIndex].value;
            var jqxhr = $.ajax({
                type: 'POST',
                url: 'person/newMI',
                data: {Ministry: Select,
                       Person: <?php echo $Details->ID; ?>,
                       Active: "Active",
                       Capacity: Capacity},
                cache: false
            })
            .done(function() {
                $.notify("Added to ministry", "success");
                $.fn.yiiGridView.update('minGrid');
            })
            .fail(function() {
            })
        });
    })
</script>