<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>
        <div class="container" id="page">

            <?php
            $this->widget('bootstrap.widgets.TbNavbar', array(
                'id' => 'mainNav',
                'brand' => 'Bedford FMC',
                'fluid' => false,
                'collapse' => true,
                'fixed' => false,
                'items' => array(
                    array(
                        'class' => 'bootstrap.widgets.TbMenu',
                        'htmlOptions' => array('class' => 'nav-pills'),
                        'type' => 'navbar',
                        'activateItems' => true,
                        'items' => array(
                            array('label' => 'Login', 'url' => array('/user/login'), 'visible' => Yii::app()->user->isGuest),
                            array('label' => 'Contributions', 'visible' => !Yii::app()->user->isGuest, 'items' => array(
                                    array('label' => 'Add and Edit', 'url' => array('/contribution/'), 'visible' => !Yii::app()->user->isGuest),
                                    array('label' => 'Report', 'url' => array('/contribution/'), 'visible' => !Yii::app()->user->isGuest)
                                )),
                            array('label' => 'Events', 'visible' => !Yii::app()->user->isGuest, 'items' => array(
                                    array('label' => 'Add and Edit', 'url' => array('/event/'), 'visible' => !Yii::app()->user->isGuest),
                                    array('label' => 'Report', 'url' => array('/event/'), 'visible' => !Yii::app()->user->isGuest)
                                )),
                            array('label' => 'Families', 'visible' => !Yii::app()->user->isGuest, 'items' => array(
                                    array('label' => 'Add and Edit', 'url' => array('/family/'), 'visible' => !Yii::app()->user->isGuest),
                                    array('label' => 'Report', 'url' => array('/family/'), 'visible' => !Yii::app()->user->isGuest)
                                )),
                            array('label' => 'Life Events', 'visible' => !Yii::app()->user->isGuest, 'items' => array(
                                    array('label' => 'Add and Edit', 'url' => array('/lifeevent/'), 'visible' => !Yii::app()->user->isGuest),
                                    array('label' => 'Report', 'url' => array('/lifeevent/'), 'visible' => !Yii::app()->user->isGuest)
                                )),
                            array('label' => 'Ministries', 'visible' => !Yii::app()->user->isGuest, 'items' => array(
                                    array('label' => 'Add and Edit', 'url' => array('/ministry/'), 'visible' => !Yii::app()->user->isGuest),
                                    array('label' => 'Report', 'url' => array('/ministry/'), 'visible' => !Yii::app()->user->isGuest)
                                )),
                            array('label' => 'People', 'visible' => !Yii::app()->user->isGuest, 'items' => array(
                                    array('label' => 'Add and Edit', 'url' => array('/person/'), 'visible' => !Yii::app()->user->isGuest),
                                    array('label' => 'Report', 'url' => array('/person/'), 'visible' => !Yii::app()->user->isGuest)
                                ))
                        ),
                    ),
                    array(
                        'class' => 'bootstrap.widgets.TbMenu',
                        'type' => 'navbar',
                        'htmlOptions' => array('class' => 'pull-right'),
                        'items' => array(
                            array('label' => 'Admin', 'visible' => Yii::app()->getModule('user')->isAdmin(), 'items' => array(
                                    array('label' => 'Manage Users', 'url' => array('/user/'), 'visible' => Yii::app()->getModule('user')->isAdmin()),
                                    array('label' => 'Manage Rights', 'url' => array('/rights/'), 'visible' => Yii::app()->getModule('user')->isAdmin())
                                )),
                            array('label' => 'Logout', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest),
                        ),
                    )
                ),
            ));
            ?>
            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('zii.widgets.CBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                ));
                ?><!-- breadcrumbs -->
            <?php endif ?>
            <div class="container">   
                <?php echo $content; ?>
            </div>
            <div class="clear"></div>

            <div id="footer" style="text-align: center;">
                Copyright &copy; 2014 - <?php echo date('Y'); ?> by Bedford Free Methodist Church.<br/>
                All Rights Reserved.<br/>
                <?php echo Yii::powered(); ?>
            </div><!-- footer -->

        </div><!-- page -->
    </body>
</html>
<script>
    $.ajaxSetup({
	global: true,
	dataFilter: function(data,type){
		//  only 'text' and 'html' dataType should be filtered
		if (type && type != "html" && type != "text")
		{
            		return data;
		}
 
		var selector = 'script[src]'; // copy-paste the following back in after [src] if something goes wrong: ,link[rel="stylesheet"]
 
      		// get loaded scripts from DOM the first time we execute.
        	if (!$._loadedScripts) {
			$._loadedScripts = {};
			$._dataHolder = $(document.createElement('div'));
 
            		var loadedScripts = $(document).find(selector);
 
			//fetching scripts from the DOM
		        for (var i = 0, len = loadedScripts.length; i < len; i++) 
			{
        		        $._loadedScripts[loadedScripts[i].src] = 1;
            		}
        	}
 
		//$._dataHolder.html(data) does not work
		$._dataHolder[0].innerHTML = data;
 
		// iterate over new scripts and remove if source is already in DOM:
		var incomingScripts = $($._dataHolder).find(selector);
		for (var i = 0, len = incomingScripts.length; i < len; i++)
		{
			if ($._loadedScripts[incomingScripts[i].src])
			{
	        	        $(incomingScripts[i]).remove();
            		}
            		else
            		{
                		$._loadedScripts[incomingScripts[i].src] = 1;
            		}
        	}
 
		return $._dataHolder[0].innerHTML;
	}
    });
</script>