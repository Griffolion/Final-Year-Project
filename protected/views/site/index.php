<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<?php 

if (!Yii::app()->user->isGuest)
{
    $head = 'Logged in as ' . Yii::app()->user->name;
    $cont = '';
}
else
{
    $head = 'BFMC Attendance Monitor';
    $cont = 'Please log in';
}

$this->widget('bootstrap.widgets.TbHeroUnit', array(
    'heading' => $head,
)); ?>