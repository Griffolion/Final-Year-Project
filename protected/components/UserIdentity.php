<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	public function authenticate()
	{
            $sql = "SELECT Username FROM users WHERE Username = :username AND Password = :password;";
            $AuthenticatorCommand = Yii::app()->db->createCommand($sql)->setFetchMode(PDO::FETCH_ASSOC);
            $AuthenticatorCommand->bindValue(":username", $this->username);
            $AuthenticatorCommand->bindValue(":password", $this->password);
            $Result = $AuthenticatorCommand->query();
            $this->errorCode = $Result->getRowCount() > 0 ? self::ERROR_NONE : self::ERROR_UNKNOWN_IDENTITY;
            return $this->errorCode;
	}
}