<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class abstractController extends Controller
{
    protected $Model, $modelName;
    
    protected function setVars()
    {
        $this->Model = new abstractModel;
        $this->modelName = 'abstractThing';
    }
        
    public function actionIndex()
    {
        $this->render('index');
    }
    
    public function actionCreate()
    {
        $this->setVars();
        
        $Model = new $this->Model;
        
        if(isset($_POST['ajax']) && $_POST['ajax']=== strtolower($this->modelName) . '-create')
        {
            echo CActiveForm::validate($Model);
            Yii::app()->end();
        }
        
        if (isset($_POST[$this->modelName]))
        {
            $this->Model->attributes = $_POST[$this->modelName];
            $this->Model->tableName = $this->modelName;
            
            if ($this->Model->validate())
            {
                $this->Model->Create();
                Yii::app()->user->setFlash('created',$this->modelName . ' created successfully.');
                $this->refresh();
            }
        }
        $this->render('create',array('model'=>$Model));
    }
    
    public function actionRead()
    {
        $this->setVars();
        
        $Model = new $this->Model;
        
        if (isset($_POST[$this->modelName]))
        {
            $Model->attributes = $_POST[$this->modelName];
            
            $Model->Read();
        }
        $this->render('read',array('model' => $Model, 'dropdown' => $Model->getIDs()));
    }
    
    public function actionUpdate()
    {
        $this->setVars();
        
        $Model = new $this->Model;
        
        if(isset($_POST['ajax']) && $_POST['ajax']=== strtolower($this->modelName) . '-update')
        {
            echo CActiveForm::validate($Model);
            Yii::app()->end();
        }
        
        if (isset($_POST[$this->modelName]))
        {
            $Model->attributes = $_POST[$this->modelName];
            
            if ($Model->validate())
            {
                $Model->Update();
                $this->refresh();
            }
        }
        $this->render('update',array('model'=>$Model));
    }
    
    public function actionDelete()
    {
        $this->setVars();
        
        $Model = new $this->Model;
        
        if (isset($_POST[$this->modelName]))
        {
            $Model->attributes = $_POST[$this->modelName];
            $Model->Delete();
            $this->refresh();
        }
        $this->render('delete', array('model'=>$Model));
    }
}