<?php

/**
 * This is the model class for table "person".
 *
 * The followings are the available columns in table 'person':
 * @property integer $ID
 * @property string $Title
 * @property string $firstName
 * @property string $middleName
 * @property string $lastName
 * @property string $DOB
 * @property string $Address1
 * @property string $Address2
 * @property string $Address3
 * @property string $City
 * @property string $ZIP
 * @property string $State
 * @property string $Occupation
 * @property string $homePhone
 * @property string $cellPhone
 * @property string $workPhone
 * @property string $homeEmail
 * @property string $workEmail
 * @property string $memberStatus
 * @property string $dateJoined
 * @property string $Gender
 * @property string $maritalStatus
 * @property string $Notes
 * @property string $dateCreated
 * @property string $dateModified
 * @property string $Active
 *
 * The followings are the available model relations:
 * @property Contribution[] $contributions
 * @property Event[] $events
 * @property Family[] $families
 * @property Lifeevent[] $lifeevents
 * @property Ministry[] $ministries
 */
class Person extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'person';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('firstName, lastName, DOB, Address1, City, ZIP, State, homePhone, memberStatus, Active, dateJoined', 'required'),
            array('Title', 'length', 'max' => 4),
            array('firstName, middleName, lastName, Address1, Address2, Address3, City, Occupation, homeEmail, workEmail', 'length', 'max' => 255),
            array('ZIP, homePhone, cellPhone, workPhone', 'length', 'max' => 10),
            array('State', 'length', 'max' => 2),
            array('memberStatus', 'length', 'max' => 19),
            array('Gender', 'length', 'max' => 6),
            array('maritalStatus', 'length', 'max' => 17),
            array('Active', 'length', 'max' => 8),
            //array('dateModified, dateCreated', 'safe')
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Title, firstName, middleName, lastName, DOB, Address1, Address2, Address3, City, ZIP, State, Occupation, homePhone, cellPhone, workPhone, homeEmail, workEmail, memberStatus, dateJoined, Gender, maritalStatus, Notes, dateCreated, dateModified, Active', 'safe'),
            array('ID, Title, firstName, middleName, lastName, DOB, Address1, Address2, Address3, City, ZIP, State, Occupation, homePhone, cellPhone, workPhone, homeEmail, workEmail, memberStatus, dateJoined, Gender, maritalStatus, Notes, dateCreated, dateModified, Active', 'safe', 'on' => 'search'),
            array('ZIP', 'ext.CustomValidators.ValidZIP'),
            array('State', 'ext.CustomValidators.ValidState')
        );
    }

    public function beforeSave() {
        return parent::beforeSave();
    }

    public function afterSave() {

        return parent::afterSave();
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
//        $model = Person::model()->findByPk(1);
//        $model->contributions
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'contributions' => array(self::HAS_MANY, 'Contribution', 'Person'),
            'events' => array(self::MANY_MANY, 'Event', 'eventattendance(Person, Event)'),
            'families' => array(self::MANY_MANY, 'Family', 'familymembership(Person, Family)'),
            'lifeevents' => array(self::MANY_MANY, 'Lifeevent', 'lifeeventoccurrance(Person, lifeEvent)'),
            'ministries' => array(self::MANY_MANY, 'Ministry', 'ministryinvolvement(Person, Ministry)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'Person Number',
            'Title' => 'Title',
            'firstName' => 'First Name',
            'middleName' => 'Middle Name',
            'lastName' => 'Last Name',
            'DOB' => 'Date of Birth',
            'Address1' => 'Address 1',
            'Address2' => 'Address 2',
            'Address3' => 'Address 3',
            'City' => 'City',
            'ZIP' => 'ZIP Code',
            'State' => 'State',
            'Occupation' => 'Occupation',
            'homePhone' => 'Home Phone',
            'cellPhone' => 'Cell Phone',
            'workPhone' => 'Work Phone',
            'homeEmail' => 'Home Email',
            'workEmail' => 'Work Email',
            'memberStatus' => 'Member Status',
            'dateJoined' => 'Date Joined',
            'Gender' => 'Gender',
            'maritalStatus' => 'Marital Status',
            'Notes' => 'Notes',
            'dateCreated' => 'Date Created',
            'dateModified' => 'Date Modified',
            'Active' => 'Active',
        );
    }

    private function getMonths() {
        $Months = array();
        // done like this so january and february can go back 2 months without breaking array
        $Months[1] = "November";
        $Months[2] = "December";
        $Months[3] = "January";
        $Months[4] = "February";
        $Months[5] = "March";
        $Months[6] = "April";
        $Months[7] = "May";
        $Months[8] = "June";
        $Months[9] = "July";
        $Months[10] = "August";
        $Months[11] = "September";
        $Months[12] = "October";
        $Months[13] = "November";
        $Months[14] = "December";
        return $Months;
    }
    
    private function determineMonths($startMonth, $allMonths) {
        $dMonths = array();
        $index = $allMonths[$startMonth];
        $dMonths[$index] = 0; // 2 months back
        $index = $allMonths[$startMonth + 1];
        $dMonths[$index + 1] = 0; // 1 month back
        $index = $allMonths[$startMonth + 2];
        $dMonths[$index + 2] = 0; // current month
        
        return $dMonths;
    }
    
    public function getThreeMonthContribution() {
        $currentMonth = (int) date('n');
        $lastMonth = $currentMonth - 1;
        $twoMonthsAgo = $currentMonth - 2;
        $year = (int) date('Y');
        
        $allMonths = $this->getMonths();
        $currentMonthName = $allMonths[$currentMonth + 2];
        $lastMonthName = $allMonths[$lastMonth + 2];
        $twoMonthsAgoName = $allMonths[$twoMonthsAgo + 2];        
        
        $criteria = new CDbCriteria();
        $criteria->addCondition('(Person = ' . $this->ID . ' AND YEAR(Date) = ' . $year . ' AND MONTH(Date) = ' . $currentMonth . ') OR (Person = ' . $this->ID . ' AND YEAR(Date) = ' . $year . ' AND MONTH(Date) = ' . $lastMonth . ') OR (Person = ' . $this->ID . ' AND YEAR(Date) = ' . $year . ' AND MONTH(Date) = ' . $twoMonthsAgo . ')');
        $criteria->order = 'Date ASC';
        
        $contributions = Contribution::model()->findAll($criteria);
        
        $workingMonths = array();
        
        if (!empty($contributions)) {
            $workingMonths[$twoMonthsAgoName] = 0;
            $workingMonths[$lastMonthName] = 0;
            $workingMonths[$currentMonthName] = 0;
            foreach ($contributions as $contr) {
                $index = ((int) date('n', strtotime($contr->Date)) + 2);
                $index2 = $allMonths[$index];
                $workingMonths[$index2] = $workingMonths[$index2] + $contr->Amount;
            }
        }
        return $workingMonths;
    }
    
    public function getFamilies() {
        $familiesArray = $this->getRelated('families');
        $families = new CArrayDataProvider($familiesArray, array('keyField' => 'ID'));
        return $families;
    }
    
    public function getAvailableFamilies()
    {
        $allFamilies = Family::model()->findAll();
        if (!empty($allFamilies)) {
            //print_r("families detected       ");
            $criteria = new CDbCriteria();
            $criteria->addCondition('Person = ' . $this->ID);
            $Memberships = FamilyMembership::model()->findAll($criteria);
            if (!empty($Memberships)) {
                //print_r("Memberships detected     ");
                $familyCriteria = new CDbCriteria();
                foreach ($Memberships as $Membership) {
                    $familyCriteria->addCondition('ID != ' . $Membership->Family);
                }
                $familyCriteria->order = 'Name ASC';
                $availableFamilies = Family::model()->findAll($familyCriteria);
                if (!empty($availableFamilies)) {
                    //print_r("returning available families     ");
                    return $availableFamilies;
                }
                else {
                    //print_r("member of all families     ");
                    return NULL;
                }
            }
            else {
                //print_r("Member of no families     ");
                return $allFamilies;
            }
        }
        else {
            //print_r("no families detected     ");
            return NULL;
        }
    }
    
    public function getEvents() {
        $eventsArray = $this->getRelated('events');
        $events = new CArrayDataProvider($eventsArray, array('keyField' => 'ID'));
        return $events;
    }
    
    
    public function getAvailableEvents() {
        $allEvents = Event::model()->findAll();
        if (!empty($allEvents)) {
            //print_r("families detected       ");
            $criteria = new CDbCriteria();
            $criteria->addCondition('Person = ' . $this->ID);
            $Attendances = EventAttendance::model()->findAll($criteria);
            if (!empty($Attendances)) {
                //print_r("Memberships detected     ");
                $eventCriteria = new CDbCriteria();
                foreach ($Attendances as $Attendance) {
                    $eventCriteria->addCondition('ID != ' . $Attendance->Event);
                }
                $eventCriteria->order = 'Name ASC';
                $availableEvents = Event::model()->findAll($eventCriteria);
                if (!empty($availableEvents)) {
                    //print_r("returning available families     ");
                    return $availableEvents;
                }
                else {
                    //print_r("member of all families     ");
                    return NULL;
                }
            }
            else {
                //print_r("Member of no families     ");
                return $allEvents;
            }
        }
        else {
            //print_r("no families detected     ");
            return NULL;
        }
    }
    
    public function getLifeOccurrences() {
        $lifeEventArray = $this->getRelated('lifeevents');
        $occurrences = new CArrayDataProvider($lifeEventArray, array('keyField' => 'ID'));
        return $occurrences;
    }
    
    public function getAvailableLifeOccurrences() {
        $allLifeEvents = Lifeevent::model()->findAll();
        if (!empty($allLifeEvents)) {
            //print_r("families detected       ");
            $criteria = new CDbCriteria();
            $criteria->addCondition('Person = ' . $this->ID);
            $Occurrences = LifeEventOccurrance::model()->findAll($criteria);
            if (!empty($Occurrences)) {
                //print_r("Memberships detected     ");
                $occurrenceCriteria = new CDbCriteria();
                foreach ($Occurrences as $Occurrence) {
                    $occurrenceCriteria->addCondition('ID != ' . $Occurrence->lifeEvent);
                }
                $occurrenceCriteria->order = 'Name ASC';
                $availableLifeEvents = Lifeevent::model()->findAll($occurrenceCriteria);
                if (!empty($availableEvents)) {
                    //print_r("returning available families     ");
                    return $availableEvents;
                }
                else {
                    //print_r("member of all families     ");
                    return NULL;
                }
            }
            else {
                //print_r("Member of no families     ");
                return $allLifeEvents;
            }
        }
        else {
            //print_r("no families detected     ");
            return NULL;
        }
    }
    
    public function getMinistries() {
        $ministryArray = $this->getRelated('ministries');
        $ministries = new CArrayDataProvider($ministryArray, array('keyField' => 'ID'));
        return $ministries;
    }
    
    public function getAvailableMinistries() {
        $allMinistries = Ministry::model()->findAll();
        if (!empty($allMinistries)) {
            //print_r("families detected       ");
            $criteria = new CDbCriteria();
            $criteria->addCondition('Person = ' . $this->ID);
            $Involvements = MinistryInvolvement::model()->findAll($criteria);
            if (!empty($Involvements)) {
                //print_r("Memberships detected     ");
                $involvementCriteria = new CDbCriteria();
                foreach ($Involvements as $Involvement) {
                    $involvementCriteria->addCondition('ID != ' . $Involvement->Ministry);
                }
                $involvementCriteria->order = 'Name ASC';
                $availableMinistries = Ministry::model()->findAll($involvementCriteria);
                if (!empty($availableMinistries)) {
                    //print_r("returning available families     ");
                    return $availableMinistries;
                }
                else {
                    //print_r("member of all families     ");
                    return NULL;
                }
            }
            else {
                //print_r("Member of no families     ");
                return $allMinistries;
            }
        }
        else {
            //print_r("no families detected     ");
            return NULL;
        }
    }
    
    public function pullRelated() {
        $relatedData = array();
        $relatedData['families'] = $this->getFamilies();
        $relatedData['availableFamilies'] = $this->getAvailableFamilies();
        $relatedData['events'] = $this->getEvents();
        $relatedData['availableEvents'] = $this->getAvailableEvents();
        $relatedData['ministries'] = $this->getMinistries();
        $relatedData['availableMinistries'] = $this->getAvailableMinistries();
        $relatedData['lifeEvents'] = $this->getLifeOccurrences();
        $relatedData['availableLifeEvents'] = $this->getAvailableLifeOccurrences();
        $relatedData['contributions'] = $this->getThreeMonthContribution();
        return $relatedData;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('Title', $this->Title, true);
        $criteria->compare('firstName', $this->firstName, true);
        $criteria->compare('middleName', $this->middleName, true);
        $criteria->compare('lastName', $this->lastName, true);
        $criteria->compare('DOB', $this->DOB, true);
        $criteria->compare('Address1', $this->Address1, true);
        $criteria->compare('Address2', $this->Address2, true);
        $criteria->compare('Address3', $this->Address3, true);
        $criteria->compare('City', $this->City, true);
        $criteria->compare('ZIP', $this->ZIP, true);
        $criteria->compare('State', $this->State, true);
        $criteria->compare('Occupation', $this->Occupation, true);
        $criteria->compare('homePhone', $this->homePhone, true);
        $criteria->compare('cellPhone', $this->cellPhone, true);
        $criteria->compare('workPhone', $this->workPhone, true);
        $criteria->compare('homeEmail', $this->homeEmail, true);
        $criteria->compare('workEmail', $this->workEmail, true);
        $criteria->compare('memberStatus', $this->memberStatus, true);
        $criteria->compare('dateJoined', $this->dateJoined, true);
        $criteria->compare('Gender', $this->Gender, true);
        $criteria->compare('maritalStatus', $this->maritalStatus, true);
        $criteria->compare('Notes', $this->Notes, true);
        $criteria->compare('dateCreated', $this->dateCreated, true);
        $criteria->compare('dateModified', $this->dateModified, true);
        $criteria->compare('Active', $this->Active, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getFullName() {
        return $this->firstName . " " . $this->lastName;
    }
    
    public function getFullAddress() {
        $full = $this->Address1;
        
        if ($this->Address2 != '') {
            $full .= ', ' . $this->Address2;
        }
        if ($this->Address3 != '') {
            $full .= ', ' . $this->Address3;
        }
        
        return $full;
    }
    
    public function getBestNumber() {
        if ($this->cellPhone != '') {
            return $this->cellPhone;
        }
        else if ($this->homePhone != '') {
            return $this->homePhone;
        }
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Person the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
