<?php

/**
 * This is the model class for table "family".
 *
 * The followings are the available columns in table 'family':
 * @property integer $ID
 * @property string $Name
 * @property string $dateCreated
 * @property string $Address1
 * @property string $Address2
 * @property string $Address3
 * @property string $City
 * @property string $ZIP
 * @property string $State
 * @property string $dateModified
 *
 * The followings are the available model relations:
 * @property Contribution[] $contributions
 * @property Person[] $people
 */
class Family extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'family';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Name, Address1, City, ZIP, State', 'required'),
            array('Name, Address1, Address2, Address3, City', 'length', 'max'=>255),
            array('ZIP', 'length', 'max'=>10),
            array('State', 'length', 'max'=>2),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Name, dateCreated, Address1, Address2, Address3, City, ZIP, State, dateModified', 'safe', 'on'=>'search'),
            array('ID, Name, dateCreated, Address1, Address2, Address3, City, ZIP, State, dateModified', 'safe'),
            array('ZIP', 'ext.CustomValidators.ValidZIP'),
            array('State', 'ext.CustomValidators.ValidState')
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'contributions' => array(self::HAS_MANY, 'Contribution', 'Family'),
            'people' => array(self::MANY_MANY, 'Person', 'familymembership(Family, Person)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'ID' => 'Family',
            'Name' => 'Family Name',
            'dateCreated' => 'Date Created',
            'Address1' => 'Address 1',
            'Address2' => 'Address 2',
            'Address3' => 'Address 3',
            'City' => 'City',
            'ZIP' => 'ZIP',
            'State' => 'State',
            'dateModified' => 'Date Modified',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('ID',$this->ID);
        $criteria->compare('Name',$this->Name,true);
        $criteria->compare('dateCreated',$this->dateCreated,true);
        $criteria->compare('Address1',$this->Address1,true);
        $criteria->compare('Address2',$this->Address2,true);
        $criteria->compare('Address3',$this->Address3,true);
        $criteria->compare('City',$this->City,true);
        $criteria->compare('ZIP',$this->ZIP,true);
        $criteria->compare('State',$this->State,true);
        $criteria->compare('dateModified',$this->dateModified,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Family the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    private function getRelatedProvider($relation) {
        $relation = $this->getRelated($relation);
        $provider = new CArrayDataProvider($relation, array('keyField' => 'ID'));
        return $provider;
    }
    
    public function getAvailablePeople() {
        $allPeople = Person::model()->findAll();
        if (!empty($allPeople)) {
            //print_r("families detected       ");
            $criteria = new CDbCriteria();
            $criteria->addCondition('Family = ' . $this->ID);
            $Memberships = FamilyMembership::model()->findAll($criteria);
            if (!empty($Memberships)) {
                //print_r("Memberships detected     ");
                $MembershipCriteria = new CDbCriteria();
                foreach ($Memberships as $Membership) {
                    $MembershipCriteria->addCondition('ID != ' . $Membership->Person);
                }
                $MembershipCriteria->order = 'firstName ASC';
                $availablePeople = Person::model()->findAll($MembershipCriteria);
                if (!empty($availablePeople)) {
                    //print_r("returning available families     ");
                    return $availablePeople;
                }
                else {
                    //print_r("member of all families     ");
                    return NULL;
                }
            }
            else {
                //print_r("Member of no families     ");
                return $allPeople;
            }
        }
        else {
            //print_r("no families detected     ");
            return NULL;
        }
    }
    
    public function pullRelated() {
        $relatedData = array();
        
        $relatedData['contributions'] = $this->getRelatedProvider('contributions');
        $relatedData['people'] = $this->getRelatedProvider('people');
        $relatedData['availablePeople'] = $this->getAvailablePeople();
        
        return $relatedData;
    }
}