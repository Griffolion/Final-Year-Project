<?php

/**
 * This is the model class for table "familymembership".
 *
 * The followings are the available columns in table 'familymembership':
 * @property integer $Person
 * @property integer $Family
 * @property string $Position
 * @property string $dateCreated
 */
class FamilyMembership extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'familymembership';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Person, Family, Position', 'required'),
			array('Person, Family', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Person, Family, dateCreated, Position', 'safe', 'on'=>'search'),
                        array('Person, Family, dateCreated, Position', 'safe')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'family' => array(self::BELONGS_TO, 'Family', 'Family'),
                    'person' => array(self::BELONGS_TO, 'Person', 'Person')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Person' => 'Person',
			'Family' => 'Family',
                        'Position' => 'Parent or Child',
			'dateCreated' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Person',$this->Person);
		$criteria->compare('Family',$this->Family);
                $criteria->compare('Position',$this->Position);
		$criteria->compare('dateCreated',$this->dateCreated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FamilyMembership the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
