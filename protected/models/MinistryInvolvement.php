<?php

/**
 * This is the model class for table "ministryinvolvement".
 *
 * The followings are the available columns in table 'ministryinvolvement':
 * @property integer $Person
 * @property integer $Ministry
 * @property string $dateCreated
 * @property string $Capacity
 * @property string $Active
 * @property string $Notes
 * @property string $dateModified
 */
class MinistryInvolvement extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ministryinvolvement';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Person, Ministry, Capacity, Active', 'required'),
			array('Person, Ministry', 'numerical', 'integerOnly'=>true),
			array('Capacity', 'length', 'max'=>9),
			array('Active', 'length', 'max'=>8),
			array('Person, Ministry, dateCreated, Capacity, Active, Notes, dateModified', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Person, Ministry, dateCreated, Capacity, Active, Notes, dateModified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Person' => 'Person',
			'Ministry' => 'Ministry',
			'dateCreated' => 'Date Created',
			'Capacity' => 'Capacity',
			'Active' => 'Active',
			'Notes' => 'Notes',
			'dateModified' => 'Date Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Person',$this->Person);
		$criteria->compare('Ministry',$this->Ministry);
		$criteria->compare('dateCreated',$this->dateCreated,true);
		$criteria->compare('Capacity',$this->Capacity,true);
		$criteria->compare('Active',$this->Active,true);
		$criteria->compare('Notes',$this->Notes,true);
		$criteria->compare('dateModified',$this->dateModified,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MinistryInvolvement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
