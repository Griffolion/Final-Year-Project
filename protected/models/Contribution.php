<?php

/**
 * This is the model class for table "contribution".
 *
 * The followings are the available columns in table 'contribution':
 * @property integer $ID
 * @property integer $Ministry
 * @property integer $Person
 * @property integer $Family
 * @property string $Date
 * @property string $Type
 * @property double $Amount
 * @property string $taxDeductible
 * @property integer $checkNumber
 * @property string $dateCreated
 *
 * The followings are the available model relations:
 * @property Family $family
 * @property Person $person
 * @property Ministry $ministry
 */
class Contribution extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'contribution';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Ministry, Person, Family, Date, Type, Amount, taxDeductible, checkNumber', 'required'),
            array('Ministry, Person, Family, checkNumber', 'numerical', 'integerOnly'=>true),
            array('Amount', 'numerical'),
            array('Type', 'length', 'max'=>21),
            array('taxDeductible', 'length', 'max'=>45),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Ministry, Person, Family, Date, Type, Amount, taxDeductible, checkNumber, dateCreated', 'safe', 'on'=>'search'),
            array('ID, Ministry, Person, Family, Date, Type, Amount, taxDeductible, checkNumber, dateCreated', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'family' => array(self::BELONGS_TO, 'Family', 'Family'),
            'person' => array(self::BELONGS_TO, 'Person', 'Person'),
            'ministry' => array(self::BELONGS_TO, 'Ministry', 'Ministry'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'ID' => 'Contribution',
            'Ministry' => 'Ministry',
            'Person' => 'Person',
            'Family' => 'Family',
            'Date' => 'Contribution Date',
            'Type' => 'Contribution Type',
            'Amount' => 'Amount',
            'taxDeductible' => 'Tax Deductible',
            'checkNumber' => 'Check Number',
            'dateCreated' => 'Date Created',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('ID',$this->ID);
        $criteria->compare('Ministry',$this->Ministry);
        $criteria->compare('Person',$this->Person);
        $criteria->compare('Family',$this->Family);
        $criteria->compare('Date',$this->Date,true);
        $criteria->compare('Type',$this->Type,true);
        $criteria->compare('Amount',$this->Amount);
        $criteria->compare('taxDeductible',$this->taxDeductible,true);
        $criteria->compare('checkNumber',$this->checkNumber);
        $criteria->compare('dateCreated',$this->dateCreated,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Contribution the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function pullRelated() {
        $relatedData = array();
        $relatedData['person'] = $this->getRelated('person');
        $relatedData['ministry'] = $this->getRelated('ministry');
        $relatedData['family'] = $this->getRelated('family');
        return $relatedData;
    }
}