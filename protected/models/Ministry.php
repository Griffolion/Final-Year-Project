<?php
/**
 * This is the model class for table "ministry".
 *
 * The followings are the available columns in table 'ministry':
 * @property integer $ID
 * @property string $Name
 * @property string $Description
 * @property string $dateCreated
 * @property string $dateModified
 *
 * The followings are the available model relations:
 * @property Contribution[] $contributions
 * @property Event[] $events
 * @property Person[] $people
 */
class Ministry extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'ministry';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Name, Description', 'required'),
            array('Name', 'length', 'max'=>45),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Name, Description, dateCreated, dateModified', 'safe', 'on'=>'search'),
            array('ID, Name, Description, dateCreated, dateModified', 'safe')
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'contributions' => array(self::HAS_MANY, 'Contribution', 'Ministry'),
            'events' => array(self::HAS_MANY, 'Event', 'Ministry'),
            'people' => array(self::MANY_MANY, 'Person', 'ministryinvolvement(Ministry, Person)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'ID' => 'Ministry ID',
            'Name' => 'Name',
            'Description' => 'Description',
            'dateCreated' => 'Date Created',
            'dateModified' => 'Date Modified',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('ID',$this->ID);
        $criteria->compare('Name',$this->Name,true);
        $criteria->compare('Description',$this->Description,true);
        $criteria->compare('dateCreated',$this->dateCreated,true);
        $criteria->compare('dateModified',$this->dateModified,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Ministry the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    private function getRelatedProvider($relation) {
        $relation = $this->getRelated($relation);
        $provider = new CArrayDataProvider($relation, array('keyField' => 'ID'));
        return $provider;
    }
    
    public function getAvailablePeople() {
        $allPeople = Person::model()->findAll();
        if (!empty($allPeople)) {
            //print_r("families detected       ");
            $criteria = new CDbCriteria();
            $criteria->addCondition('Ministry = ' . $this->ID);
            $Involvements = MinistryInvolvement::model()->findAll($criteria);
            if (!empty($Involvements)) {
                //print_r("Memberships detected     ");
                $involvementCriteria = new CDbCriteria();
                foreach ($Involvements as $Involvement) {
                    $involvementCriteria->addCondition('ID != ' . $Involvement->Person);
                }
                $involvementCriteria->order = 'firstName ASC';
                $availablePeople = Person::model()->findAll($involvementCriteria);
                if (!empty($availablePeople)) {
                    //print_r("returning available families     ");
                    return $availablePeople;
                }
                else {
                    //print_r("member of all families     ");
                    return NULL;
                }
            }
            else {
                //print_r("Member of no families     ");
                return $allPeople;
            }
        }
        else {
            //print_r("no families detected     ");
            return NULL;
        }
    }
    
    public function pullRelated() {
        $relatedData = array();
        
        $relatedData['events'] = $this->getRelatedProvider('events');
        $relatedData['contributions'] = $this->getRelatedProvider('contributions');
        $relatedData['people'] = $this->getRelatedProvider('people');
        $relatedData['availablePeople'] = $this->getAvailablePeople();
        
        return $relatedData;
    }
}