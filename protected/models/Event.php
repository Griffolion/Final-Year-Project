<?php

/**
 * This is the model class for table "event".
 *
 * The followings are the available columns in table 'event':
 * @property integer $ID
 * @property integer $Ministry
 * @property string $Name
 * @property string $Start
 * @property string $End
 * @property string $dateCreated
 * @property string $dateModified
 *
 * The followings are the available model relations:
 * @property Ministry $ministry
 * @property Person[] $people
 */
class Event extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'event';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Ministry, Name, Start, End,', 'required'),
            array('Ministry', 'numerical', 'integerOnly'=>true),
            array('Name', 'length', 'max'=>255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Ministry, Name, Start, End, dateCreated, dateModified', 'safe', 'on'=>'search'),
            array('ID, Ministry, Name, Start, End, dateCreated, dateModified', 'safe')
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'ministry' => array(self::BELONGS_TO, 'Ministry', 'Ministry'),
            'people' => array(self::MANY_MANY, 'Person', 'eventattendance(Event, Person)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'ID' => 'Event ID',
            'Ministry' => 'Ministry',
            'Name' => 'Event Name',
            'Start' => 'Event Start',
            'End' => 'Event End',
            'dateCreated' => 'Date Created',
            'dateModified' => 'Date Modified',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('ID',$this->ID);
        $criteria->compare('Ministry',$this->Ministry);
        $criteria->compare('Name',$this->Name,true);
        $criteria->compare('Start',$this->Start,true);
        $criteria->compare('End',$this->End,true);
        $criteria->compare('dateCreated',$this->dateCreated,true);
        $criteria->compare('dateModified',$this->dateModified,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Event the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    private function getRelatedProvider($relation) {
        $relation = $this->getRelated($relation);
        $provider = new CArrayDataProvider($relation, array('keyField' => 'ID'));
        return $provider;
    }
    
    public function getAvailablePeople() {
        $allPeople = Person::model()->findAll();
        if (!empty($allPeople)) {
            //print_r("families detected       ");
            $criteria = new CDbCriteria();
            $criteria->addCondition('Event = ' . $this->ID);
            $Attendances = EventAttendance::model()->findAll($criteria);
            if (!empty($Attendances)) {
                //print_r("Memberships detected     ");
                $attendanceCriteria = new CDbCriteria();
                foreach ($Attendances as $Attendance) {
                    $attendanceCriteria->addCondition('ID != ' . $Attendance->Person);
                }
                $attendanceCriteria->order = 'firstName ASC';
                $availablePeople = Person::model()->findAll($attendanceCriteria);
                if (!empty($availablePeople)) {
                    //print_r("returning available families     ");
                    return $availablePeople;
                }
                else {
                    //print_r("member of all families     ");
                    return NULL;
                }
            }
            else {
                //print_r("Member of no families     ");
                return $allPeople;
            }
        }
        else {
            //print_r("no families detected     ");
            return NULL;
        }
    }
    
    public function pullRelated() {
        $relatedData = array();
        
        $relatedData['ministry'] = $this->getRelated('ministry');
        $relatedData['people'] = $this->getRelatedProvider('people');
        $relatedData['availablePeople'] = $this->getAvailablePeople();
        
        return $relatedData;
    }
}