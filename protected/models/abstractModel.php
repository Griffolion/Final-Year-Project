<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class abstractModel extends CFormModel
{
    public $tableName, $idName, $modelID;
    
    public function rules()
    {
        return array(
            array('tableName, idName, modelID', 'unsafe')
        );
    }
    
    public function Save($SQL, $pArray, $return = FALSE)
    {
        $Command = Yii::app()->db->createCommand($SQL)->setFetchMode(PDO::FETCH_ASSOC);
        
        if ($pArray !== NULL)
        {
            foreach ($pArray as $key => $value)
            {
                $Command->bindValue(':' . $key, $value);
            }
        }
        
        $Results = $Command->execute();
        
        if ($return)
        {
            return $Results;
        }
    }
    
    public function Create()
    {
        $sql = 'INSERT INTO ' . strtolower($this->tableName) . ' (';
        $sql2 = ' VALUES(';
        $sql3 = ')';
        
        $attr = $this->attributes;
        
        $paramArray = array();
        
        foreach ($attr as $key => $value)
        {
            if ($value != '' && $key != 'tableName')
            {
                $paramArray[$key] = $value;
                $sql .= $key . ',';
                $sql2 .= ':' . $key . ',';
            }
        }
        
        $sql = rtrim($sql,',');
        $sql .= ')';
        $sql2 = rtrim($sql2,',');
        
        $finalSQL = $sql . $sql2 . $sql3;

        $this->Save($finalSQL, $paramArray);
    }
    
    public function Read()
    {   
        $SQL = 'SELECT * FROM ' . $this->tableName . ' WHERE personID = ' . $this->modelID;
        
        $this->attributes = $this->Save($SQL, NULL, TRUE);
    }
    
    public function Update()
    {   
        $sql = 'UPDATE ' . $this->tableName . ' SET (';
        $sql2 = ') VALUES(';
        $sql3 = ') WHERE ' . $this->idName . ' = ' . $this->modelID;
        
        $attr = $this->attributes;
        $paramArray = array();

        foreach ($attr as $key => $value)
        {
            if ($value != '' && $key != 'tableName')
            {
                $paramArray[$key] = $value;
                $sql .= $key . ',';
                $sql2 .= ':' . $key . ',';
            }
        }
        
        $sql = rtrim($sql,',');
        $sql2 = rtrim($sql2,',');
        
        $finalSQL = $sql . $sql2 . $sql3;

        $this->Save($finalSQL, $paramArray);
    }
    
    public function Delete()
    {
        $SQL = 'DELETE FROM ' . $this->tableName . ' WHERE ' . $this->idName . ' = ' . $this->modelID;
        $this->Save($SQL, NULL);
    }
    
    public function getIDs()
    {
        $SQL = 'SELECT ' . $this->idName . ' FROM ' . $this->tableName . ' ORDER BY ' . $this->idName;
        return $this->Save($SQL,NULL,TRUE);
    }
    
    public function setVars($Name, $ID, $mID)
    {
        $this->tableName = $Name;
        $this->modelID = $ID;
        $this->idName = $mID;
    }
}