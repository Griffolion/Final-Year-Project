<?php

class LifeeventController extends RController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
            'rights', // rights management
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view','loadModelAjax'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update','updateChild','deleteLinkRecord','newLEO'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }
    
    public function actionDeleteLinkRecord($thisID, $linkData, $linkName, $Entity) {
        if (!Yii::app()->request->isPostRequest || !Yii::app()->request->isAjaxRequest) {
            throw new CHttpException(400, 'Invalid request.');
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('Family = ' . $thisID . ' && ' . $linkName . ' = ' . $linkData);
        switch ($Entity) {
            case "Event":
                $toDelete = EventAttendance::model()->find($criteria);
                break;
            case "Family":
                $toDelete = FamilyMembership::model()->find($criteria);
                break;
            case "LifeEvent":
                $toDelete = LifeEventOccurrance::model()->find($criteria);
                break;
            case "Ministry":
                $toDelete = MinistryInvolvement::model()->find($criteria);
                break;
            case "Contribution":
                $toDelete = Contribution::model()->find($criteria);
                break;
            default:
                throw new CHttpException(400, 'Entity not recognised');
        }
        $toDelete->delete();
    }
    
    public function actionNewLEO() {
        if (!Yii::app()->request->isPostRequest || !Yii::app()->request->isAjaxRequest) {
            throw new CHttpException(400, 'Invalid request.');
        }
        if (isset($_POST['Person']) && isset($_POST['LifeEvent']) && isset($_POST['Date'])) {
            $Link = new LifeEventOccurrance();
            $Link->setAttribute('Person', $_POST['Person']);
            $Link->setAttribute('lifeEvent', $_POST['LifeEvent']);
            $Link->setAttribute('dateOfOccurrance', $_POST['Date']);
            if ($Link->validate()) {
                if (!$Link->save()) {
                    throw new CHttpException(400, 'Link record did not save, record may already exist');
                }
            }
            else {
                throw new CHttpException(400, 'Link record did not validate');
            }
        }
        else {
            throw new CHttpException(400, 'Parameters not set.');
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Lifeevent;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        
        // If new life event entry is posted
        if (isset($_POST['Lifeevent'])) {
            $params = array(); 
            parse_str($_POST['Lifeevent'], $params); // Parsing JSON object back to PHP array
            $model->attributes = $params['Lifeevent']; // Massive assignment to model from JSON parsed array
            if ($model->validate()) {
                $command = Yii::app()->db->createCommand();
                $command->insert('lifeevent', // $model->save() didn't work, threw that memory leak error we saw before
                    array( 'Name' => $model->Name // So we had to resort to the good old fashioned way
                         , 'Notes' => $model->Notes));
                /* To my knowledge, there's no decent way to get back a full 
                 * Life Event model after inserting to DB, so I had to get the model
                 * by selecting the row with the latest dateCreated stamp */
                $Details = Lifeevent::model()->findBySql("SELECT * FROM lifeevent "
                        . "ORDER BY dateCreated DESC "
                        . "LIMIT 1;");
                /* Why couldn't I just put through $model? Good question, all I 
                 * know is that it didn't work */
                $this->renderPartial('_viewAjax', array(
                'Details' => $Details,
                'relatedData' => $Details->pullRelated()
                    ), false, true);
            } else {
                throw new CHttpException(400, 'The new life event failed to validate, check input');
            }
            die();
        }
        
        //If wanting to create a new record
        if (isset($_POST['create'])) {
            $model = new Lifeevent();
            $this->renderPartial('_form', array(
                'model' => $model
                    ), false, true);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate()
    {
        //check if the correct data is posted 
        if (isset($_POST['scenario'])) {
            // Getting the values from Yii::app() request functionality, rather straight from _POST
            $val = Yii::app()->request->getParam('value');
            $pk = Yii::app()->request->getParam('pk');
            $attribute = Yii::app()->request->getParam('name');

            if (empty($attribute)) { // Throw exception if the attribute name is undefined for whatever reason - if this happens, it might be an error with js
                throw new CException(Yii::t('TbEditableSaver.editable', 'Property "attribute" should be defined.'));
            }
            if (empty($pk)) { // Throw exception if the primary key is undefined for whatever reason - if this happens, it might be an error with js
                throw new CException(Yii::t('TbEditableSaver.editable', 'Property "primaryKey" should be defined.'));
            }

            $model = CActiveRecord::model('Lifeevent')->findByPk($pk); // Initialise a model straight to the existing one based on the PK
            $model->setAttribute($attribute, $val); // Set the attribute in question to the new value
            $model->validate(); // Validate the model so as to generate any potential new errors from the attribute change

            if ($model->hasErrors()) { // If the model contains errors (boolean)
                $msg = ''; // String initialised to contain error messages
                foreach ($model->getErrors($attribute) as $attribute => $error) { // For each error for the attribute in question
                    $msg .= $error . "\n"; // Append msg with the error and a new line tag so as to display multiple potential errors
                }
                throw new CHttpException(400, $msg); // Throw a new CHttpException with our message
            }

            //If model validated with the new value
            $command = Yii::app()->db->createCommand(); // create a new DB command
            // Update the database
            $command->update('lifeevent', // UPDATE tablename
                    array($attribute => $val,), // ATTRIBUTES ...
                    'ID = :id', // WHERE ...
                    array(':id' => $pk)); // VALUES ...
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete()
    {
        if (isset($_POST["pk"])) {
                if ($this->loadModel($_POST["pk"])->delete()) {
                    $this->renderPartial('_viewAjaxDeleted', array(
                            ), false, true);
                }
            }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Lifeevent');
        $model = new Lifeevent('search');
        $model->unsetAttributes();
        if (isset($_GET['Lifeevent'])) {
            $model->attributes = $_GET['Lifeevent'];
        }
        
        $this->render('index', array('model' => $model,
            'dataProvider' => $dataProvider
        ));
    }
    
    public function actionUpdateChild() {
        if (isset($_POST['pk'])) {
            $Details = Lifeevent::model()->findByPk($_POST['pk']);
            $this->renderPartial('_viewAjax', array(
                'Details' => $Details,
                'relatedData' => $Details->pullRelated()
            ), false, true);
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new Lifeevent('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Lifeevent'])) {
            $model->attributes=$_GET['Lifeevent'];
        }

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Lifeevent the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Lifeevent::model()->findByPk($id);
        if ($model===null) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Lifeevent $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax']==='lifeevent-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}