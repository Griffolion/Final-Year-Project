<?php

class PersonController extends RController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
            'rights', // rights management
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'loadmodelajax'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'updateChild', 'newFM', 'newEA', 'newLE', 'newMI', 'DeleteLinkRecord'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }
    
    public function actionDeleteLinkRecord($thisID, $linkData, $linkName, $Entity) {
        if (!Yii::app()->request->isPostRequest || !Yii::app()->request->isAjaxRequest) {
            throw new CHttpException(400, 'Invalid request.');
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('Person = ' . $thisID . ' && ' . $linkName . ' = ' . $linkData);
        $toDelete;
        switch ($Entity) {
            case "Event":
                $toDelete = EventAttendance::model()->find($criteria);
                break;
            case "Family":
                $toDelete = FamilyMembership::model()->find($criteria);
                break;
            case "LifeEvent":
                $toDelete = LifeEventOccurrance::model()->find($criteria);
                break;
            case "Ministry":
                $toDelete = MinistryInvolvement::model()->find($criteria);
                break;
            case "Contribution":
                $toDelete = Contribution::model()->find($criteria);
                break;
            default:
                throw new CHttpException(400, 'Entity not recognised');
        }
        $toDelete->delete();
    }
        
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Person;

        
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Person'])) {
            $params = array(); 
            parse_str($_POST['Person'], $params); // Parsing JSON object back to PHP array
            $model->attributes = $params['Person']; // Massive assignment to model from JSON parsed array
            if ($model->validate()) {
                $command = Yii::app()->db->createCommand();
                $command->insert('person', // $model->save() didn't work, threw that memory leak error we saw before
                    array( 'Title' => $model->Title // So we had to resort to the good old fashioned way
                         , 'firstName' => $model->firstName
                         , 'middleName' => $model->middleName
                         , 'lastName' => $model->lastName
                         , 'DOB' => $model->DOB
                         , 'Address1' => $model->Address1
                         , 'Address2' => $model->Address2
                         , 'Address3' => $model->Address3
                         , 'City' => $model->City
                         , 'ZIP' => $model->ZIP
                         , 'State' => $model->State
                         , 'Occupation' => $model->Occupation
                         , 'homePhone' => $model->homePhone
                         , 'cellPhone' => $model->cellPhone
                         , 'workPhone' => $model->workPhone
                         , 'homeEmail' => $model->homeEmail
                         , 'workEmail' => $model->workEmail
                         , 'memberStatus' => $model->memberStatus
                         , 'dateJoined' => $model->dateJoined
                         , 'Gender' => $model->Gender
                         , 'maritalStatus' => $model->maritalStatus
                         , 'Notes' => $model->Notes
                         , 'Active' => $model->Active,));
                /* To my knowledge, there's no decent way to get back a full 
                 * Person model after inserting to DB, so I had to get the model
                 * by selecting the row with the latest dateCreated stamp */
                $Details = Person::model()->findBySql("SELECT * FROM person "
                        . "ORDER BY dateCreated DESC "
                        . "LIMIT 1;");
                /* Why couldn't I just put through $model? Good question, all I 
                 * know is that it didn't work */
                $this->renderPartial('_viewAjax', array(
                'Details' => $Details,
                'relatedData' => $Details->pullRelated(),
                'createSuccess' => true,
                    ),false, true);
            } else {
                print_r($params);
                echo "failure";
            }
            die();
        }
        
        //If wanting to create a new record
        if (isset($_POST['create'])) {
            $model = new Person();
            $this->renderPartial('_form', array(
                'model' => $model,
                    ),false, true);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate() {
        if (isset($_POST['scenario'])) {
            // Getting the values from Yii::app() request functionality, rather straight from _POST
            $val = Yii::app()->request->getParam('value');
            $pk = Yii::app()->request->getParam('pk');
            $attribute = Yii::app()->request->getParam('name');

            if (empty($attribute)) { // Throw exception if the attribute name is undefined for whatever reason - if this happens, it might be an error with js
                throw new CException(Yii::t('TbEditableSaver.editable', 'Property "attribute" should be defined.'));
            }
            if (empty($pk)) { // Throw exception if the primary key is undefined for whatever reason - if this happens, it might be an error with js
                throw new CException(Yii::t('TbEditableSaver.editable', 'Property "primaryKey" should be defined.'));
            }

            $model = CActiveRecord::model('Person')->findByPk($pk); // Initialise a model straight to the existing one based on the PK
            $model->setAttribute($attribute, $val); // Set the attribute in question to the new value
            $model->validate(); // Validate the model so as to generate any potential new errors from the attribute change

            if ($model->hasErrors()) { // If the model contains errors (boolean)
                $msg = ''; // String initialised to contain error messages
                foreach ($model->getErrors($attribute) as $attribute => $error) { // For each error for the attribute in question
                    $msg .= $error . "\n"; // Append msg with the error and a new line tag so as to display multiple potential errors
                }
                throw new CHttpException(400, $msg); // Throw a new CHttpException with our message
            }

            //If model validated with the new value
            $command = Yii::app()->db->createCommand(); // create a new DB command
            // Update the database
            $command->update('person', // UPDATE tablename
                    array($attribute => $val,), // ATTRIBUTES ...
                    'ID = :id', // WHERE ...
                    array(':id' => $pk)); // VALUES ...
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will load in the AJAX deletion page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete() {
        // we only allow deletion via POST request
        if (isset($_POST["pk"])) {
            $p = CActiveRecord::model('Person')->findByPk($_POST['pk']);
            if ($p->Active == 'Inactive') {
                throw new CHttpException(400,'Person is already deactivated');
            }
            else {
                $p->setAttribute('Active', 'Inactive');
                if ($p->save()) {
                    $this->renderPartial('_viewAjaxDeleted', array(
                        'deleted' => true
                            ),false, true);
                }
                else {
                    throw new CHttpException(400,'Error saving change back to database');
                }
            }
        }
        else {
            throw new CHttpException(400,'Delete denied: request must be POST');
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Person');
        $model = new Person('search');
        $model->unsetAttributes();

        if (isset($_GET['Person'])) {
            $model->attributes = $_GET['Person'];
        }
        
        $this->render('index', array('model' => $model,
            'dataProvider' => $dataProvider
        ));
    }
    
    public function actionNewFM() {
        if (isset($_POST['Family']) && isset($_POST['Person'])) {
            $Link = new FamilyMembership();
            $Link->Family = $_POST['Family'];
            $Link->Person = $_POST['Person'];
            $Link->save();
        }
    }
    
    public function actionNewMI() {
        if (isset($_POST['Ministry']) && isset($_POST['Person']) && isset($_POST['Capacity'])) {
            $Link = new MinistryInvolvement();
            $Link->setAttribute('Ministry', $_POST['Ministry']);
            $Link->setAttribute('Person', $_POST['Person']);
            $Link->setAttribute('Active', 'Active');
            $Link->setAttribute('Capacity',$_POST['Capacity']);
            $Link->save();
        }
    }
    
    public function actionNewEA() {
        if (isset($_POST['Event']) && isset($_POST['Person'])) {
            $Link = new EventAttendance();
            $Link->Event = $_POST['Event'];
            $Link->Person = $_POST['Person'];
            $Link->save();
        }
    }
    
    public function actionNewLE() {
        if (isset($_POST['lifeEvent']) && isset($_POST['Person'])) {
            $Link = new LifeEventOccurrance();
            $Link->lifeEvent = $_POST['lifeEvent'];
            $Link->Person = $_POST['Person'];
            $Link->save();
        }
    }
    
    public function actionUpdateChild() {
        // Wanting to view an existing person
        if (isset($_POST['pk'])) {
            $Details = Person::model()->findByPk($_POST['pk']);
            $relatedData = $Details->pullRelated();
            $this->renderPartial('_viewAjax', array(
                'Details' => $Details,
                'relatedData' => $relatedData,
                    ),false, true);
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        
        $model = new Person('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Person'])) {
            $model->attributes = $_GET['Person'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionLoadModelAjax($id) {
        $this->renderPartial('_viewAjax', array('model' => $this->loadModel($id)), false, true);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Person the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Person::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Person $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'person-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
