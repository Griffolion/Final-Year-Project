<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ValidZIP extends CValidator
{
    private $Pattern = '/^[0-9]{5}((?:[-\\s][0-9]{4})?)$/';
    
    protected function validateAttribute($object, $attribute) {
        $value = $object->$attribute;
        if (!preg_match($this->Pattern,$value))
        {
            $this->addError($object,$attribute, 'You must enter a valid US ZIP code');
        }
    }
    
    public function clientValidateAttribute($object,$attribute)
    {
        $pattern = $this->Pattern;
        $condition="!value.match({$pattern})";

        return "
        if(".$condition.") {
        messages.push(".CJSON::encode('You must enter a valid US ZIP code').");
        }
        ";
    }
}