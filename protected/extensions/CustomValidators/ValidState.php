<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ValidState extends CValidator
{
    private $Pattern = '/^((A[LKZR])|(C[AOT])|(D[EC])|(FL)|(GA)|(HI)|(I[DLNA])|(K[SY])|(LA)|(M[EDAINSOT])|(N[EVHJMYCD])|(O[HKR])|(PA)|(RI)|(S[CD])|(T[NX])|(UT)|(V[TA])|(W[AVIY]))$/';
    
    protected function validateAttribute($object, $attribute) {
        $value = $object->$attribute;
        if (!preg_match($this->Pattern,$value))
        {
            $this->addError($object,$attribute, 'You must enter a valid US state according to the two letter abbreviation system');
        }
    }
    
    public function clientValidateAttribute($object,$attribute)
    {
        $pattern = $this->Pattern;
        $condition="!value.match({$pattern})";
        
        return "
        if(".$condition.") {
        messages.push(".CJSON::encode('You must enter a valid US state according to the two letter abbreviation system').");
        }
        ";
    }
}