-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 16, 2014 at 06:31 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `testproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `authassignment`
--

CREATE TABLE IF NOT EXISTS `authassignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authassignment`
--

INSERT INTO `authassignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('Admin', '1', NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `authitem`
--

CREATE TABLE IF NOT EXISTS `authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authitem`
--

INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('Admin', 2, NULL, NULL, 'N;'),
('Authenticated', 2, NULL, NULL, 'N;'),
('Guest', 2, NULL, NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `authitemchild`
--

CREATE TABLE IF NOT EXISTS `authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contribution`
--

CREATE TABLE IF NOT EXISTS `contribution` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Ministry` int(11) NOT NULL,
  `Person` int(11) NOT NULL,
  `Family` int(11) NOT NULL,
  `Date` date NOT NULL,
  `Type` enum('Cash','Check','Regular Bank Transfer') NOT NULL,
  `Amount` float NOT NULL,
  `taxDeductible` varchar(45) NOT NULL,
  `checkNumber` int(11) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `contributionID_UNIQUE` (`ID`),
  KEY `fk_Contribution_Ministry1_idx` (`Ministry`),
  KEY `fk_Contribution_Individuals1_idx` (`Person`),
  KEY `fk_Contribution_Family1_idx` (`Family`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Ministry` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Start` date NOT NULL,
  `End` date NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `fk_Event_Ministry1_idx` (`Ministry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Triggers `event`
--
DROP TRIGGER IF EXISTS `Event_BUPD`;
DELIMITER //
CREATE TRIGGER `Event_BUPD` BEFORE UPDATE ON `event`
 FOR EACH ROW SET NEW.dateModified = CURRENT_TIMESTAMP
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `eventattendance`
--

CREATE TABLE IF NOT EXISTS `eventattendance` (
  `Event` int(11) NOT NULL,
  `Person` int(11) NOT NULL,
  `dateAttended` date NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Event`,`Person`),
  KEY `fk_eventAttendance_Individuals1_idx` (`Person`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `eventattendance`
--
DROP TRIGGER IF EXISTS `eventAttendance_BUPD`;
DELIMITER //
CREATE TRIGGER `eventAttendance_BUPD` BEFORE UPDATE ON `eventattendance`
 FOR EACH ROW SET NEW.dateModified = CURRENT_TIMESTAMP
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `family`
--

CREATE TABLE IF NOT EXISTS `family` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Address1` varchar(255) NOT NULL,
  `Address2` varchar(255) NOT NULL,
  `Address3` varchar(255) NOT NULL,
  `City` varchar(255) NOT NULL,
  `ZIP` varchar(10) NOT NULL,
  `State` varchar(2) NOT NULL,
  `dateModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `familyID_UNIQUE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Triggers `family`
--
DROP TRIGGER IF EXISTS `Family_BUPD`;
DELIMITER //
CREATE TRIGGER `Family_BUPD` BEFORE UPDATE ON `family`
 FOR EACH ROW SET NEW.dateModified = CURRENT_TIMESTAMP
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `familymembership`
--

CREATE TABLE IF NOT EXISTS `familymembership` (
  `Person` int(11) NOT NULL,
  `Family` int(11) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Position` enum('Parent','Child') NOT NULL,
  PRIMARY KEY (`Family`,`Person`),
  KEY `fk_familyMembership_Family1_idx` (`Family`),
  KEY `fk_familyMembership_Individuals1` (`Person`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lifeevent`
--

CREATE TABLE IF NOT EXISTS `lifeevent` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Notes` varchar(255) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lifeEventID_UNIQUE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lifeeventoccurrance`
--

CREATE TABLE IF NOT EXISTS `lifeeventoccurrance` (
  `lifeEvent` int(11) NOT NULL,
  `Person` int(11) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateOfOccurrance` date NOT NULL,
  `Notes` longtext NOT NULL,
  PRIMARY KEY (`lifeEvent`,`Person`),
  KEY `fk_lifeEventOccurrance_Individuals1_idx` (`Person`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ministry`
--

CREATE TABLE IF NOT EXISTS `ministry` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) NOT NULL,
  `Description` longtext NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Triggers `ministry`
--
DROP TRIGGER IF EXISTS `Ministry_BUPD`;
DELIMITER //
CREATE TRIGGER `Ministry_BUPD` BEFORE UPDATE ON `ministry`
 FOR EACH ROW SET NEW.dateModified = CURRENT_TIMESTAMP
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `ministryinvolvement`
--

CREATE TABLE IF NOT EXISTS `ministryinvolvement` (
  `Person` int(11) NOT NULL,
  `Ministry` int(11) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Capacity` enum('Leader','Volunteer') NOT NULL,
  `Active` enum('Active','Inactive') NOT NULL,
  `Notes` longtext NOT NULL,
  `dateModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Ministry`,`Person`),
  KEY `fk_ministryInvolvement_Ministry1_idx` (`Ministry`),
  KEY `fk_ministryInvolvement_Individuals1` (`Person`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `ministryinvolvement`
--
DROP TRIGGER IF EXISTS `ministryInvolvement_BUPD`;
DELIMITER //
CREATE TRIGGER `ministryInvolvement_BUPD` BEFORE UPDATE ON `ministryinvolvement`
 FOR EACH ROW SET NEW.dateModified = CURRENT_TIMESTAMP
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE IF NOT EXISTS `person` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` enum('Mr','Mrs','Miss','Ms','Dr') NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `middleName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `DOB` date NOT NULL,
  `Address1` varchar(255) NOT NULL,
  `Address2` varchar(255) NOT NULL,
  `Address3` varchar(255) NOT NULL,
  `City` varchar(255) NOT NULL,
  `ZIP` varchar(10) NOT NULL,
  `State` varchar(2) NOT NULL,
  `Occupation` varchar(255) NOT NULL,
  `homePhone` varchar(10) NOT NULL,
  `cellPhone` varchar(10) NOT NULL,
  `workPhone` varchar(10) NOT NULL,
  `homeEmail` varchar(255) NOT NULL,
  `workEmail` varchar(255) NOT NULL,
  `memberStatus` enum('Attending','Attending Regularly','Church Member','Conference Member','Shut-in') NOT NULL,
  `dateJoined` date NOT NULL,
  `Gender` enum('Female','Male') NOT NULL,
  `maritalStatus` enum('Engaged','In a Relationship','Married','Single') NOT NULL,
  `Notes` longtext NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Active` enum('Active','Inactive') NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `individualID_UNIQUE` (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`ID`, `Title`, `firstName`, `middleName`, `lastName`, `DOB`, `Address1`, `Address2`, `Address3`, `City`, `ZIP`, `State`, `Occupation`, `homePhone`, `cellPhone`, `workPhone`, `homeEmail`, `workEmail`, `memberStatus`, `dateJoined`, `Gender`, `maritalStatus`, `Notes`, `dateCreated`, `dateModified`, `Active`) VALUES
(1, 'Mr', 'Andrew', 'Stephen', 'Hall', '1989-01-16', '617 Q Street', '', '', 'Bedford', '47421', 'IN', 'Student', '8125082281', '', '', '', '', 'Church Member', '2014-06-01', 'Male', 'Engaged', '', '2014-09-16 16:29:25', '2014-09-16 16:29:25', 'Active');

--
-- Triggers `person`
--
DROP TRIGGER IF EXISTS `Person_BUPD`;
DELIMITER //
CREATE TRIGGER `Person_BUPD` BEFORE UPDATE ON `person`
 FOR EACH ROW SET NEW.dateModified = CURRENT_TIMESTAMP
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(50) NOT NULL DEFAULT '',
  `firstname` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`user_id`, `lastname`, `firstname`) VALUES
(1, 'Admin', 'Administrator'),
(2, 'Demo', 'Demo');

-- --------------------------------------------------------

--
-- Table structure for table `profiles_fields`
--

CREATE TABLE IF NOT EXISTS `profiles_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` varchar(15) NOT NULL DEFAULT '0',
  `field_size_min` varchar(15) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` varchar(5000) NOT NULL DEFAULT '',
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` varchar(5000) NOT NULL DEFAULT '',
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `varname` (`varname`,`widget`,`visible`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `profiles_fields`
--

INSERT INTO `profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES
(1, 'lastname', 'Last Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect Last Name (length between 3 and 50 characters).', '', '', '', '', 1, 3),
(2, 'firstname', 'First Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect First Name (length between 3 and 50 characters).', '', '', '', '', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `rights`
--

CREATE TABLE IF NOT EXISTS `rights` (
  `itemname` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`itemname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`),
  KEY `superuser` (`superuser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `activkey`, `create_at`, `lastvisit_at`, `superuser`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'webmaster@example.com', '9a24eff8c15a6a141ece27eb6947da0f', '2014-09-15 17:56:40', '2014-09-16 02:45:01', 1, 1),
(2, 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 'demo@example.com', '099f825543f7850cc038b90aaff39fac', '2014-09-15 17:56:40', '0000-00-00 00:00:00', 0, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `authassignment`
--
ALTER TABLE `authassignment`
  ADD CONSTRAINT `authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `authitemchild`
--
ALTER TABLE `authitemchild`
  ADD CONSTRAINT `authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `contribution`
--
ALTER TABLE `contribution`
  ADD CONSTRAINT `fk_Contribution_Family1` FOREIGN KEY (`Family`) REFERENCES `family` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contribution_Individuals1` FOREIGN KEY (`Person`) REFERENCES `person` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contribution_Ministry1` FOREIGN KEY (`Ministry`) REFERENCES `ministry` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `fk_Event_Ministry1` FOREIGN KEY (`Ministry`) REFERENCES `ministry` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `eventattendance`
--
ALTER TABLE `eventattendance`
  ADD CONSTRAINT `fk_eventAttendance_Event1` FOREIGN KEY (`Event`) REFERENCES `event` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_eventAttendance_Individuals1` FOREIGN KEY (`Person`) REFERENCES `person` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `familymembership`
--
ALTER TABLE `familymembership`
  ADD CONSTRAINT `fk_familyMembership_Family1` FOREIGN KEY (`Family`) REFERENCES `family` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_familyMembership_Individuals1` FOREIGN KEY (`Person`) REFERENCES `person` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lifeeventoccurrance`
--
ALTER TABLE `lifeeventoccurrance`
  ADD CONSTRAINT `fk_lifeEventOccurrance_Individuals1` FOREIGN KEY (`Person`) REFERENCES `person` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_lifeEventOccurrance_lifeEvent1` FOREIGN KEY (`lifeEvent`) REFERENCES `lifeevent` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ministryinvolvement`
--
ALTER TABLE `ministryinvolvement`
  ADD CONSTRAINT `fk_ministryInvolvement_Individuals1` FOREIGN KEY (`Person`) REFERENCES `person` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ministryInvolvement_Ministry1` FOREIGN KEY (`Ministry`) REFERENCES `ministry` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `user_profile_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `rights`
--
ALTER TABLE `rights`
  ADD CONSTRAINT `rights_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
